//
//  UiLabelExtension.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 04/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

extension UILabel {
  func setSizeFont (sizeFont: CGFloat) {
    self.font =  UIFont(name: self.font.fontName, size: sizeFont)!
    self.sizeToFit()
  }
}