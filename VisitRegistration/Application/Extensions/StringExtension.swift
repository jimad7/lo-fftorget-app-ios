//
//  DateExtension.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import FontAwesome_swift

public extension String {
  
  func dateFromStringWithFormat(format: String) -> NSDate? {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = format
    if self.hasSuffix("CET") || self.hasSuffix("CEDT") || self.hasSuffix("CEST") {
      //For en_us locale, iOS does not parse timezone CET due to ambiguity - since it's not used, drop it
      let dateWithoutTimeZone = String(self.hasSuffix("CET") ? self.characters.dropLast(3) : self.characters.dropLast(4))
      return dateFormatter.dateFromString(dateWithoutTimeZone)
    }
    return dateFormatter.dateFromString(self)
  }
  
  func iconForType() -> String {
    return self == VisitType.Single.rawValue ? String.fontAwesomeIconWithName(FontAwesome.User) : String.fontAwesomeIconWithName(FontAwesome.Users)
  }
  
  func iconForInfoType() -> String {
    if self == InfoType.Single.rawValue {
      return String.fontAwesomeIconWithName(FontAwesome.User)
    } else if self == InfoType.Group.rawValue {
      return String.fontAwesomeIconWithName(FontAwesome.Users)
    } else if self == InfoType.Support.rawValue {
      return String.fontAwesomeIconWithName(FontAwesome.QuestionCircle)
    } else if self == InfoType.Signup.rawValue {
      return String.fontAwesomeIconWithName(FontAwesome.Edit)
    }
    return ""
  }
  
  var URLEncoded:String {
    let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~"
    let unreservedCharset = NSCharacterSet(charactersInString: unreservedChars)
    let encodedString = self.stringByAddingPercentEncodingWithAllowedCharacters(unreservedCharset)
    return encodedString ?? self
  }
  
  public func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String? {
    let unreserved = "*-._"
    let allowed = NSMutableCharacterSet.alphanumericCharacterSet()
    allowed.addCharactersInString(unreserved)
    
    if plusForSpace {
      allowed.addCharactersInString(" ")
    }
    
    var encoded = stringByAddingPercentEncodingWithAllowedCharacters(allowed)
    if plusForSpace {
      encoded = encoded?.stringByReplacingOccurrencesOfString(" ",
        withString: "+")
    }
    return encoded
  }
  
  func containsAnyLetters() -> Bool {
    for chr in self.characters {
      if ((chr >= "a" && chr <= "ö") || (chr >= "A" && chr <= "Ö") ) {
        return true
      }
    }
    return false
  }
  
  func isPhoneNumeric() -> Bool {
    let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "*", " ", "-"]
    return Set(self.characters).isSubsetOf(nums)
  }
}