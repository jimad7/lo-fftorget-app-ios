//
//  NSUrlExtension.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

extension NSURL {
  
  func URLByAppendingQueryConfigParameters() -> NSURL {
    guard let urlComponents = NSURLComponents(URL: self, resolvingAgainstBaseURL: true) else {
        return self
    }
    
    var mutableQueryItems: [NSURLQueryItem] = urlComponents.queryItems ?? []
    mutableQueryItems.append(NSURLQueryItem(name: "OpenAgent", value: "1"))
    mutableQueryItems.append(NSURLQueryItem(name: "id", value: ProfileData.id))
    
    urlComponents.queryItems = mutableQueryItems
    
    return urlComponents.URL!
  }
 
  func URLByAppendingQueryParameters(parameters: [String: AnyObject]?) -> NSURL {
    guard let parameters = parameters,
      urlComponents = NSURLComponents(URL: self, resolvingAgainstBaseURL: true) else {
        return self
    }
    
    var mutableQueryItems: [NSURLQueryItem] = urlComponents.queryItems ?? []
    mutableQueryItems.append(NSURLQueryItem(name: "OpenAgent", value: "1"))
    if Utils.sharedInstance.isLoggedIn() {
      mutableQueryItems.append(NSURLQueryItem(name: "id", value: ProfileData.id))
    }
    
    mutableQueryItems.appendContentsOf(parameters.map{ NSURLQueryItem(name: $0, value: $1 as? String) })
    urlComponents.queryItems = mutableQueryItems
    
    return urlComponents.URL!
  }
}
