//
//  DateExtension.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

public extension NSDate {
  
  func stringFromDateWithFormat(format: String) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = NSTimeZone(name: "CEDT")
    return dateFormatter.stringFromDate(self)
  }
}