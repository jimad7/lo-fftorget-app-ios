//
//  StoreManager.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 07/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB
import TinySQLite

class StoreManager {
    static let sharedInstance = StoreManager()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    func addOrUpdateSupportTicketLocally(values: [String: Any?]) -> Int
    {
        let db = SwiftyDB(databaseName: "fftorget")
        var nextId = (maxElement(SupportTicket.self) ?? SupportTicket())._id + 1
        
        let uni = SupportTicket(attributes: values)!
        
        let result = db.objectsForType(SupportTicket.self, matchingFilter: ["_id": uni._id])
        if let collection = result.value where !collection.isEmpty {
            uni._id = collection[0]._id
        } else {
            uni._id = nextId
            nextId += 1
        }
        db.addObject(uni, update: true)
        return uni._id
    }
    
    func deleteInfoContainer(infoType: String, id: Int)
    {
        if infoType == InfoType.Single.rawValue || infoType == InfoType.Group.rawValue {
            deleteVisit(id)
        } else if infoType == InfoType.Signup.rawValue {
            deleteSignup(id, resetVisitConnection: true)
        } else if infoType == InfoType.Support.rawValue {
            deleteSupportTicket(id)
        }
    }
    
    func deleteSignup(id: Int, resetVisitConnection: Bool)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let uni = signup(id)
        
        if uni != nil {
            db.deleteObjectsForType(Signup.self, matchingFilter: ["_id": uni!._id])
            if resetVisitConnection {
                updateVisitWithSignupId(uni!.visitid, signupid: 0)
            }
        }
    }
    
    func deleteSupportTicket(id: Int)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let uni = supportTicket(id)
        
        if uni != nil {
            db.deleteObjectsForType(SupportTicket.self, matchingFilter: ["_id": uni!._id])
        }
    }
    
    func deleteVisit(id: Int)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let uni = visit(id)
        
        if uni != nil {
            db.deleteObjectsForType(Visit.self, matchingFilter: ["_id": uni!._id])
        }
    }
    
    func addOrUpdateDistricts(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.deleteObjectsForType(District.self)
        
        let districts = json["districts"] as! [[String : AnyObject]]
        var nextId = (maxElement(District.self) ?? District())._id + 1
        
        for district in districts {
            let uni = District(attributes: district)!
            let result = db.objectsForType(District.self, matchingFilter: ["unid": uni.unid])
            if let collection = result.value where !collection.isEmpty {
                uni._id = collection[0]._id
            } else {
                uni._id = nextId
                nextId += 1
            }
            db.addObject(uni, update: true)
        }
    }
    
    func updateVisitWithSignupId(visitId: Int, signupid: Int)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let result = db.objectsForType(Visit.self, matchingFilter: ["_id": visitId])
        if let collection = result.value where !collection.isEmpty {
            let visit = collection[0]
            visit.signupid = signupid
            db.addObject(visit, update: true)
        }
    }
    
    func updateVisits(json: AnyObject, ids: [Int])
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let unids = json["id"] as! [String]
        var index = 0
        for id in ids {
            let result = db.objectsForType(Visit.self, matchingFilter: ["_id": id])
            if let collection = result.value where !collection.isEmpty {
                let visit = collection[0]
                visit.unid = unids[index]
                visit.status = VisitStatus.Synced.rawValue
                db.addObject(visit, update: true)
            }
            index += 1
        }
    }
    
    func addOrUpdateVisits(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = json["visits"] as! [[String : AnyObject]]
        var nextId = (maxElement(Visit.self) ?? Visit())._id + 1
        var unidList: [SQLiteValue?] = []
        
        for item in items {
            let uni = Visit(attributes: item)!
            let result = db.objectsForType(Visit.self, matchingFilter: ["unid": uni.unid])
            if let collection = result.value where !collection.isEmpty {
                uni._id = collection[0]._id
            } else {
                uni._id = nextId
                nextId += 1
            }
            unidList.append(uni.unid!)
            uni.status = VisitStatus.Synced.rawValue
            db.addObject(uni, update: true)
        }
        
        // if !unidList.isEmpty {
        let filter = Filter.notContains("unid", array: unidList)
            .equal("status", value: VisitStatus.Synced.rawValue)
        db.deleteObjectsForType(Visit.self, matchingFilter: filter)
        // }
    }
    
    func addOrUpdateVisitLocally(values: [String: Any?]) -> Int
    {
        let db = SwiftyDB(databaseName: "fftorget")
        var nextId = (maxElement(Visit.self) ?? Visit())._id + 1
        
        let uni = Visit(attributes: values)!
        
        let result = db.objectsForType(Visit.self, matchingFilter: ["_id": uni._id])
        if let collection = result.value where !collection.isEmpty {
            uni._id = collection[0]._id
        } else {
            uni._id = nextId
            nextId += 1
        }
        db.addObject(uni, update: true)
        return uni._id
    }
    
    func addGroupVisitsLocally(values: [String: Any?], numberOfGroups: Int) -> [Visit]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        var visits: [Visit] = []
        for groupNo in 1...numberOfGroups {
            var nextId = (maxElement(Visit.self) ?? Visit())._id + 1
            let uni = Visit(attributes: values, groupNo: groupNo)!
            
            let result = db.objectsForType(Visit.self, matchingFilter: ["_id": uni._id])
            if let collection = result.value where !collection.isEmpty {
                uni._id = collection[0]._id
            } else {
                uni._id = nextId
                nextId += 1
            }
            db.addObject(uni, update: true)
            visits.append(uni)
        }
        
        return visits
    }
    
    func addOrUpdateSignupLocally(values: [String: Any?]) -> Int
    {
        let db = SwiftyDB(databaseName: "fftorget")
        var nextId = (maxElement(Signup.self) ?? Signup())._id + 1
        
        let uni = Signup(attributes: values)!
        
        let result = db.objectsForType(Signup.self, matchingFilter: ["_id": uni._id])
        if let collection = result.value where !collection.isEmpty {
            uni._id = collection[0]._id
        } else {
            uni._id = nextId
            nextId += 1
        }
        db.addObject(uni, update: true)
        return uni._id
    }
    
    func signupForVisit(visitid: Int) -> Bool
    {
        let db = SwiftyDB(databaseName: "fftorget")
        
        let result = db.objectsForType(Visit.self, matchingFilter: ["_id": visitid])
        if let collection = result.value where !collection.isEmpty {
            let visit = collection[0]
            return visit.signupid > 0 || visit.sent == 1
        }
        return false
    }
    
    func addOrUpdateProfile(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let nextId = (maxElement(Profile.self) ?? Profile())._id + 1
        let uni = Profile(attributes: json as! [String : AnyObject])!
        let result = db.objectsForType(Profile.self, matchingFilter: ["unid": uni.unid])
        if let collection = result.value where !collection.isEmpty {
            uni._id = collection[0]._id
        } else {
            uni._id = nextId
        }
        db.addObject(uni, update: true)
        ProfileData.fullname = uni.fullname!
        ProfileData.id = uni.unid!
        ProfileData.name = uni.name!
        ProfileData.email = uni.email!
    }
    
    func addOrUpdateUsername(unid: String, username: String)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let nextId = (maxElement(Profile.self) ?? Profile())._id + 1
        var uni: Profile?
        let result = db.objectsForType(Profile.self, matchingFilter: ["unid": unid])
        if let collection = result.value where !collection.isEmpty {
            uni = collection[0]
            uni!.username = username
        } else {
            uni = Profile(username: username, unid: unid)
            uni!._id = nextId
        }
        db.addObject(uni!, update: true)
    }
    
    func addOrUpdateSettingsLocally(values: [String: Any?]) -> Int
    {
        let db = SwiftyDB(databaseName: "fftorget")
        var nextId = (maxElement(Settings.self) ?? Settings())._id + 1
        
        let uni = Settings(attributes: values)!
        
        let result = db.objectsForType(Settings.self, matchingFilter: ["_id": uni._id])
        if let collection = result.value where !collection.isEmpty {
            uni._id = collection[0]._id
        } else {
            uni._id = nextId
            nextId += 1
        }
        db.addObject(uni, update: true)
        return uni._id
    }
    
    func dataDownload(unid:String) -> DownloadData?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(DownloadData.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            
            
            
            return collection[0]
        } else {
            return nil
        }
    }

    
    func updateLatestDownloadDate(values: [String: Any?], unid: String) {
        let db = SwiftyDB(databaseName: "fftorget")
        
       //  let uni = DownloadData(attributes: values)!
        let nextId = (maxElement(DownloadData.self) ?? DownloadData())._id + 1
        var uni: DownloadData?
        let result = db.objectsForType(DownloadData.self, matchingFilter: ["unid": unid])
        if let collection = result.value where !collection.isEmpty {
            uni = collection[0]
            uni!.lastDatadownload = NSDate()
        } else {
            uni = DownloadData(unid: unid)
            uni!._id = nextId
            uni!.lastDatadownload = NSDate()
        }
         db.addObject(uni!, update: true)
           }
    
    
    func addOrUpdateProjects(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.deleteObjectsForType(Project.self)
        
        let items = json["projects"] as! [[String : AnyObject]]
        var nextId = (maxElement(Project.self) ?? Project())._id + 1
        
        for item in items {
            let uni = Project(attributes: item)!
            let result = db.objectsForType(Project.self, matchingFilter: ["unid": uni.unid])
            if let collection = result.value where !collection.isEmpty {
                uni._id = collection[0]._id
            } else {
                uni._id = nextId
                nextId += 1
            }
            db.addObject(uni, update: true)
        }
    }
    
    func addOrUpdateUnions(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.deleteObjectsForType(DbUnion.self)
        
        let unions = json["unions"] as! [[String : AnyObject]]
        var nextId = (maxElement(DbUnion.self) ?? DbUnion())._id + 1
        
        for union in unions {
            let uni = DbUnion(attributes: union)!
            let result = db.objectsForType(DbUnion.self, matchingFilter: ["unid": uni.unid])
            if let collection = result.value where !collection.isEmpty {
                uni._id = collection[0]._id
            } else {
                uni._id = nextId
                nextId += 1
            }
            db.addObject(uni, update: true)
        }
    }
    
    func addOrUpdateSections(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.deleteObjectsForType(DbSection.self)
        
        let sections = json["sections"] as! [[String : AnyObject]]
        var nextId = (maxElement(DbSection.self) ?? DbSection())._id + 1
        
        for section in sections {
            let item = DbSection(attributes: section)!
            let result = db.objectsForType(DbSection.self, matchingFilter: ["unid": item.unid])
            if let collection = result.value where !collection.isEmpty {
                item._id = collection[0]._id
                item.unionId = collection[0].unionId
            } else {
                item._id = nextId
                nextId += 1
                let components = item.unid!.componentsSeparatedByString("-") //Section unid, e.g. 2-1-0-3
                let unionUnid = components[0] + "-" + components[1] //The first two parts matches union unid, e.g. 2-1
                let unionResult = db.objectsForType(DbUnion.self, matchingFilter: ["unid": unionUnid])
                if let collection = unionResult.value where !collection.isEmpty {
                    item.unionId = collection[0]._id
                }
            }
            
            db.addObject(item, update: true)
        }
    }
    
    func addOrUpdateEmployersAndWorksites(json: AnyObject)
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.deleteObjectsForType(Worksite.self)
        db.deleteObjectsForType(Employer.self)
        
        let employers = json["workplaces"] as! [[String : AnyObject]]
        var nextId = (maxElement(Employer.self) ?? Employer())._id + 1
        var nextSiteId = (maxElement(Worksite.self) ?? Worksite())._id + 1
        
        for employer in employers {
            let emp = Employer(attributes: employer)!
            let result = db.objectsForType(Employer.self, matchingFilter: ["unid": emp.unid])
            if let collection = result.value where !collection.isEmpty {
                emp._id = collection[0]._id
            } else {
                emp._id = nextId
                nextId += 1
            }
            db.addObject(emp, update: true)
            
            //NB: Since unid for site is employer name (site name), if the site name changes in the back-end,
            //we will have two (or more) sites with the same employer id
            let site = Worksite(attributes: employer)! //values are in the employer json dict
            site.employerid = emp._id
            let siteResult = db.objectsForType(Worksite.self, matchingFilter: ["unid": site.unid])
            if let collection = siteResult.value where !collection.isEmpty {
                site._id = collection[0]._id
            } else {
                site._id = nextSiteId
                nextSiteId += 1
            }
            db.addObject(site, update: true)
        }
    }
    
    func visit(id: Int) -> Visit?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Visit.self, matchingFilter: ["_id": id])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    func signup(id:Int) -> Signup?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Signup.self, matchingFilter:["_id": id])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func profile(unid:String) -> Profile?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Profile.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func dbunion(unid:String) -> DbUnion?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(DbUnion.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func dbsection(unid:String) -> DbSection?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(DbSection.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func district(unid:String) -> District?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(District.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func project(unid:String) -> Project?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Project.self, matchingFilter:["unid": unid])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func supportTicket(id:Int) -> SupportTicket?
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(SupportTicket.self, matchingFilter:["_id": id])
        if let collection = items.value where !collection.isEmpty {
            return collection[0]
        } else {
            return nil
        }
    }
    
    func allProjects() -> [Project]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Project.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allVisits() -> [Visit]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Visit.self)
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allSupportTickets() -> [SupportTicket]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(SupportTicket.self)
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    
    func allSignups() -> [Signup]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Signup.self)
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allInfo() -> [AnyObject]
    {
        let supportTickets = allSupportTickets()
        let signups = allSignups()
        let visits = allVisits().filter({$0.status == VisitStatus.LocallySaved.rawValue})
        var all = [AnyObject]()
        for ticket in supportTickets {
            all.append(ticket)
        }
        for signup in signups {
            all.append(signup)
        }
        for visit in visits {
            all.append(visit)
        }
        return all
    }
    
    func allWorksites() -> [Worksite]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let sites = db.objectsForType(Worksite.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = sites.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allUnions() -> [DbUnion]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(DbUnion.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allDistricts() -> [District]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(District.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func allSettings() -> [Settings]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Settings.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection
        } else {
            return []
        }
    }
    
    func synchronizeAutomatically() -> Bool
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Settings.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection[0].synchronization
        } else {
            return true
        }
    }
    
    func numberOfVisits() -> Int
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(Settings.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        if let collection = items.value where !collection.isEmpty {
            return collection[0].numberOfVisits
        } else {
            return 5
        }
    }
    
    //NB: assumes union name is unique - otherwise needs to be re-implemented
    func sectionsForUnion(unionid: String) -> [DbSection]
    {
        let db = SwiftyDB(databaseName: "fftorget")
        let items = db.objectsForType(DbUnion.self, matchingFilter: ["unid": unionid])
        var id = 0
        if let collection = items.value where !collection.isEmpty {
            id = collection[0]._id
            
            let items = db.objectsForType(DbSection.self, matchingFilter: ["unionid": id])
            if let collection = items.value where !collection.isEmpty {
                return collection
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func addEmployers(employers: [Employer])
    {
        let db = SwiftyDB(databaseName: "fftorget")
        db.addObjects(employers, update: true)
    }
    
    func maxElement<D where D: Storable, D: NSObject, D:Comparable> (type: D.Type) -> D? {
        let db = SwiftyDB(databaseName: "fftorget")
        let result = db.objectsForType(D.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        
        if let collection = result.value where !collection.isEmpty {
            return collection.maxElement()
        } else {
            return nil
        }
    }
    
    func nextIdForType<D where D: Storable, D: NSObject> (type: D.Type) -> Int {
        let db = SwiftyDB(databaseName: "fftorget")
        var nextId: Int = 0
        let result = db.objectsForType(Employer.self, matchingFilter: Filter.greaterThan("_id", value: 0))
        
        if let collection = result.value where !collection.isEmpty {
            let latestAddition = collection.maxElement()
            nextId = latestAddition!._id + 1
        } else {
            return 1
        }
        return nextId
    }
}