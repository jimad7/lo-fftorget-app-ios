//
//  Utils.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

class Utils {
  static let sharedInstance = Utils()
  private init() {} //This prevents others from using the default '()' initializer for this class.
  
  func isLoggedIn() -> Bool {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    return defaults.boolForKey(defaultsKeys.loggedin)
  }
  
  func stringForKey(key: String) -> String {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    if let value = defaults.stringForKey(key) {
      return value
    } else {
      return ""
    }
  }

  func boolForKey(key: String) -> Bool {
    let defaults = NSUserDefaults.standardUserDefaults()
    return defaults.boolForKey(key)
  }
  
  func setValueForKey(key: String, value: AnyObject?) {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    defaults.setValue(value, forKey: key)
  }
  
  func deleteKey(key: String) {
    let defaults = NSUserDefaults.standardUserDefaults()
    
    defaults.removeObjectForKey(key)
  }
}