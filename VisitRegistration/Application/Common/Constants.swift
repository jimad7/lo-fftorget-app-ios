//
//  Constants.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 04/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

struct Config {
    //static let baseURL = "https://fftorget.lo.se/home/lo/fftorget/app.nsf"
    
    //static let baseURL = "https://fftorget.lo.se/home/lo/fftorget/app.nsf"
    static let baseURL = "https://fftorgetutb.lo.se/home/lo/fftorget/demo/app.nsf"
    
    static let baseRegURL = "https://fftorget.lo.se/home/lo/userreg.nsf"
    static let splineReticulatorName = "foobar"
    static let AppName = "Ff-Torget"
    static let dateFormat = "yyyy-MM-dd HH:mm:ss"
    static let shortDateFormat = "yyyy-MM-dd"
    static let numberOfVisits = 5
}

struct Color {
    static let primaryColor = UIColor(red: 0.22, green: 0.58, blue: 0.29, alpha: 1.0)
    static let secondaryColor = UIColor.lightGrayColor()
}

//These should reflect the logged-in users profile values
struct ProfileData {
    static var id = ""
    static var fullname = ""
    static var name = ""
    static var email = ""
}

enum defaultsKeys {
    static let userid = "userid"
    static let loggedin = "loggedin"
    static let name = "name"
    static let fullname = "fullname"
    static let email = "email"
}
