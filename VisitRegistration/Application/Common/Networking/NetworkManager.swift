
//
//  NetworkManager.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 06/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

enum CustomError: ErrorType {
  case SupportTicket(message: String)
  case Signup(message: String)
  case Visit(message: String)
  case PasswordReset(message: String)
}

class NetworkManager {
  static let sharedInstance = NetworkManager()
  private init() {} //This prevents others from using the default '()' initializer for this class.
  
  func receiveData(url: String, parameters: [String: AnyObject]?, completion: (Result<AnyObject, NSError>) -> Void) {
    
    Alamofire.request(.GET, "\(Config.baseURL)/\(url)", parameters: parameters)
      //.authenticate(user: "johndoe", password: "password")
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        completion(response.result)
    }
  }
  
  func sendData(url: String, parameters: [String: AnyObject]?, completion: (Result<AnyObject, NSError>) -> Void) {
    let nsurl = NSURL(string: "\(Config.baseURL)/\(url)")?.URLByAppendingQueryConfigParameters()
    Alamofire.request(.POST, nsurl!, parameters: parameters)
      .validate(statusCode: 200..<300)
//      .responseString{ response in
//        let test = ""
//      }
      .responseJSON { response in
        completion(response.result)
    }
  }
  
  func sendQuerystringData(url: String, parameters: [String: AnyObject]?, completion: (Result<AnyObject, NSError>) -> Void) {
    let nsurl = NSURL(string: "\(Config.baseURL)/\(url)")?.URLByAppendingQueryParameters(parameters)
    Alamofire.request(.POST, nsurl!)
      .validate(statusCode: 200..<300)
//            .responseString{ response in
//              var test = response
//            }
      .responseJSON { response in
        completion(response.result)
    }
  }
    
    func sendQuerystringRegistrationData(url: String, parameters: [String: AnyObject]?, completion: (Result<AnyObject, NSError>) -> Void) {
        let nsurl = NSURL(string: "\(Config.baseRegURL)/\(url)")?.URLByAppendingQueryParameters(parameters)
        Alamofire.request(.POST, nsurl!)
            .responseJSON { response in
                completion(response.result)
        }
    }

    
    
  func receiveMockData(url: String, parameters: [String: AnyObject]?, completion: (Result<AnyObject, NSError>) -> Void) {
    
//    let JSON = "{\"success\":true,\"lastError\":\"\",\"maxCount\":3452,\"workplaces\":[{\"id\":\"09DC4FFFD5A7E714C1257D690037520B\",\"name\":\"-\",\"site\":\"-\"},{\"id\":\"4FE1EA139D10B68CC1257ED2002DFE1D\",\"name\":\"Ab karl hedin torpa trä\",\"site\":\"torpa\"},{\"id\":\"CC60B215ED005058C1257E3400314719\",\"name\":\"byggföretag\",\"site\":\"Landvetter flygplats\"},{\"id\":\"3632A288B44A3E8CC1257EF6005D5936\",\"name\":\"COOP lagret västerås\",\"site\":\"Lager, Västerås\"}]}"
  let JSON = "{\"user\":\"7D8CCB03BC8A69BEC1257F3E0051C50E\",\"success\":true,\"lastError\":\"\",\"name\":\"Johan Rudström\",\"fullname\":\"CN=Johan Rudström/OU=FFTorget/O=LO/C=SE\",\"email\":\"johan.rudstrom@tromb.com\",\"phone\":\"070-00000000\",\"union\":\"Byggnads\",\"district\":\"LO-Distrikt Norra Sverige\",\"project\":\"Informatör\"}"
    let data = JSON.dataUsingEncoding(NSUTF8StringEncoding)!
    let attributes = try! NSJSONSerialization.JSONObjectWithData(data, options: []) as! [String: AnyObject]
    completion(.Success(attributes))
  }
  
  func isValid(json: AnyObject) -> Bool
  {
    guard json["success"]! != nil else {
      return false
    }
    return (json["success"] as! Bool) && ((json["lastError"]! == nil) || (json["lastError"] as! String) == "")
  }
  
    func isRegValid(json: AnyObject) -> Bool
    {
        guard json["success"]! != nil else {
            return false
        }
        
        return (json["success"] as! String) == "" && ((json["msg"]! == nil) || (json["msg"] as! String) == "")
    }
    
   
    func retrieveProfileData(completion: (RequestStatus) -> Void) {
        let dispatchGroup = dispatch_group_create()
        var status = RequestStatus()
        
        dispatch_group_enter(dispatchGroup)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        self.receiveData("getProfile", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
            status = self.parseResponse(result, url: "getProfile")
            dispatch_group_leave(dispatchGroup)
        })
        
        dispatch_group_notify(dispatchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            completion(status)
        })
        
    }

    
  func retrieveAllData(completion: (RequestStatus) -> Void) {
    let dispatchGroup = dispatch_group_create()
    var status = RequestStatus()
    
    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getProfile", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getProfile")
      dispatch_group_leave(dispatchGroup)
    })

    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getDistrictsList", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getDistrictsList")
      dispatch_group_leave(dispatchGroup)
    })

    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getProjectsList", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getProjectsList")
      dispatch_group_leave(dispatchGroup)
    })
    
    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getWorkplacesList", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getWorkplacesList")
      dispatch_group_leave(dispatchGroup)
    })
    
    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getUnionsList", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getUnionsList")
      dispatch_group_leave(dispatchGroup)
    })
    
    dispatch_group_enter(dispatchGroup)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    self.receiveData("getSectionsList", parameters: ["OpenAgent": "1", "id" : ProfileData.id], completion: { (result) -> Void in
      status = self.parseResponse(result, url: "getSectionsList")
      let max = StoreManager.sharedInstance.numberOfVisits()
      self.receiveData("getMyVisits", parameters: ["OpenAgent": "1", "id" : ProfileData.id, "max" : max], completion: { (result) -> Void in
        status = self.parseResponse(result, url: "getMyVisits")
        dispatch_group_leave(dispatchGroup)
      })
    })
    
    dispatch_group_notify(dispatchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
      completion(status)
    })

  }
  
  func parseResponse(result: Result<AnyObject, NSError>, url: String) -> RequestStatus{
    let status = RequestStatus()
    switch result {
    case .Success(let JSON):
      if !NetworkManager.sharedInstance.isValid(JSON) {
        status.message = JSON["lastError"] as? String
      } else {
        if url.containsString("getUnionsList") {
          StoreManager.sharedInstance.addOrUpdateUnions(JSON)
        } else if url.containsString("getDistrictsList") {
          StoreManager.sharedInstance.addOrUpdateDistricts(JSON)
        } else if url.containsString("getSectionsList") {
          StoreManager.sharedInstance.addOrUpdateSections(JSON)
        } else if url.containsString("getProjectsList") {
          StoreManager.sharedInstance.addOrUpdateProjects(JSON)
        } else if url.containsString("getWorkplacesList") {
          StoreManager.sharedInstance.addOrUpdateEmployersAndWorksites(JSON)
        } else if url.containsString("getMyVisits") {
          StoreManager.sharedInstance.addOrUpdateVisits(JSON)
        } else if url.containsString("getProfile") {
          StoreManager.sharedInstance.addOrUpdateProfile(JSON)
        }
    }
    case .Failure(let error):
      status.statusCode = RequestStatusCode.Error
      if let error = error as NSError? {
        if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
          status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
        } else {
          status.message = error.localizedDescription
        }
      }
    }
    return status
  }
  
  func synchronizeAllData(completion: (RequestStatus) -> Void) {

    let status = RequestStatus()
    
    self.tryToSendAllSupportTickets()
      .then { _ in
        self.tryToSendAllSignups()
      }
      .then { _ in
        self.tryToSendAllVisits()
      }
      .then { _ in
        self.getMyVisits()
      }
      .then { _ in
        completion(status)
      }
      .error {  (error: ErrorType) in
        if case let CustomError.SupportTicket(message) = error {
          status.message = "Support: \(message)"
        } else if case let CustomError.Signup(message) = error {
          status.message = "Rådgivning: \(message)"
        } else if case let CustomError.Visit(message) = error {
          status.message = "Samtal: \(message)"
        }
        status.statusCode = .Error
        completion(status)
      }
  }
  
  //MARK: - Visit retrieval
  func getMyVisits() -> Promise<AnyObject>  {
    return Promise{ fulfill, reject in
      let max = StoreManager.sharedInstance.numberOfVisits()
      self.receiveData("getMyVisits", parameters: ["OpenAgent": "1", "id" : ProfileData.id, "max" : max], completion: { (result) -> Void in
        let status = self.parseResponse(result, url: "getMyVisits")
        if status.statusCode == .Ok {
          fulfill(status)
        } else {
          reject(CustomError.Visit(message: status.message!))
        }
      })
    }
  }
  
  //MARK: - Signup send sequence
  func tryToSendAllSignups() -> Promise<AnyObject>  {
    let allSignups = StoreManager.sharedInstance.allSignups()
    
    return Promise{ fulfill, reject in
      self.sendSignupSequence(allSignups, completionHandler: { (result) -> () in
        if result.statusCode == .Error {
          reject(CustomError.Signup(message: result.message!))
        } else {
          fulfill(result)
        }
      })
    }
  }
  
  func sendSignupSequence(signups:[Signup], completionHandler:(result: RequestStatus) -> ()){
    let status = RequestStatus()
    if signups.count == 0 { // The base case
      completionHandler(result: status)
    } else {
      let signup = signups[0]
      let newSignups = Array(signups[1..<signups.count])
      sendSignup(signup) { requestResult in
        if requestResult.statusCode == .Error {
          completionHandler(result: requestResult)
        } else {
          self.sendSignupSequence(newSignups, completionHandler:completionHandler)
        }
      }
    }
  }
  
  func sendSignup(signup:Signup, completion: (RequestStatus)-> ()){
    NetworkManager.sharedInstance.sendQuerystringData("postMailFolksam", parameters: signup.dataToSynchronize()!, completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if !NetworkManager.sharedInstance.isValid(JSON) {
          status.message = JSON["lastError"] as? String
          status.statusCode = .Error
          completion(status)
        } else {
          //Ok response, remove the signup from the db
          StoreManager.sharedInstance.deleteSignup(signup._id, resetVisitConnection: false)
        }
      case .Failure(let error):
        if let error = error as NSError? {
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
          } else {
            status.message = error.localizedDescription
          }
        }
        status.statusCode = .Error
      }
      completion(status)
    })
  }
  
  //MARK: - Support ticket send sequence
  func tryToSendAllSupportTickets() -> Promise<AnyObject>  {
    let allSupportTickets = StoreManager.sharedInstance.allSupportTickets()
    
    return Promise{ fulfill, reject in
      self.sendTicketSequence(allSupportTickets, completionHandler: { (result) -> () in
        if result.statusCode == .Error {
          reject(CustomError.SupportTicket(message: result.message!))
        } else {
          fulfill(result)
        }
      })
    }
  }
  
  func sendTicketSequence(tickets:[SupportTicket], completionHandler:(result: RequestStatus) -> ()){
    let status = RequestStatus()
    if tickets.count == 0 { // The base case
      completionHandler(result: status)
    } else {
      let ticket = tickets[0]
      let newTickets = Array(tickets[1..<tickets.count])
      sendTicket(ticket) { requestResult in
        if requestResult.statusCode == .Error {
          completionHandler(result: requestResult)
        } else {
          self.sendTicketSequence(newTickets, completionHandler:completionHandler)
        }
      }
    }
  }
  
  func sendTicket(ticket:SupportTicket, completion: (RequestStatus)-> ()){
      NetworkManager.sharedInstance.sendQuerystringData("postSupport", parameters: ticket.dataToSynchronize()!, completion: { (result) -> Void in
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        let status = RequestStatus()
        switch result {
        case .Success(let JSON):
          if !NetworkManager.sharedInstance.isValid(JSON) {
            status.message = JSON["lastError"] as? String
            status.statusCode = .Error
            completion(status)
          } else {
            //Ok response, remove the ticket from the db
            StoreManager.sharedInstance.deleteSupportTicket(ticket._id)
          }
        case .Failure(let error):
          if let error = error as NSError? {
            if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
              status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
            } else {
              status.message = error.localizedDescription
            }
          }
          status.statusCode = .Error
        }
        completion(status)
      })
  }
  
  //MARK: - Visit send sequence
  func tryToSendAllVisits() -> Promise<AnyObject>  {
    let allVisits = StoreManager.sharedInstance.allVisits().filter({$0.status == VisitStatus.LocallySaved.rawValue})
    
    return Promise{ fulfill, reject in
      self.sendVisitSequence(allVisits, completionHandler: { (result) -> () in
        if result.statusCode == .Error {
          reject(CustomError.Visit(message: result.message!))
        } else {
          fulfill(result)
        }
      })
    }
  }
  
  func sendVisitSequence(visits:[Visit], completionHandler:(result: RequestStatus) -> ()){
    let status = RequestStatus()
    if visits.count == 0 { // The base case
      completionHandler(result: status)
    } else {
      let visit = visits[0]
      let newVisits = Array(visits[1..<visits.count])
      sendVisit(visit) { requestResult in
        if requestResult.statusCode == .Error {
          completionHandler(result: requestResult)
        } else {
          self.sendVisitSequence(newVisits, completionHandler:completionHandler)
        }
      }
    }
  }
  
  func sendVisit(visit:Visit, completion: (RequestStatus)-> ()){
    NetworkManager.sharedInstance.sendData("setMote", parameters: visit.dataToSynchronize()!, completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if let _ = JSON["id"]! {
          StoreManager.sharedInstance.updateVisits(JSON, ids: [visit._id])
        } else {
          status.message = NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren")
          status.statusCode = .Error
        }
      case .Failure(let error):
        if let error = error as NSError? {
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
          } else {
            status.message = error.localizedDescription
          }
        }
        status.statusCode = .Error
      }
      completion(status)
    })
    
  }
    /*
    
     func sendPasswordReset(forgotPassword:ForgotPassword, completion: (RequestStatus)-> ()){
     NetworkManager.sharedInstance.sendData("resetPassword", parameters: forgotPassword.dataToSynchronize()!, completion: { (result) -> Void in
     UIApplication.sharedApplication().networkActivityIndicatorVisible = false
     let status = RequestStatus()
     switch result {
     case .Success(let JSON):
     if let _ = JSON["success"]! {
     
     } else {
     status.message = NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren")
     status.statusCode = .Error
     }
     
     case .Failure(let error):
     if let error = error as NSError? {
     if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
     status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
     } else {
     status.message = error.localizedDescription
     }
     }
     status.statusCode = .Error
     }
     completion(status)
     })
        
    }
    */
    
    
}