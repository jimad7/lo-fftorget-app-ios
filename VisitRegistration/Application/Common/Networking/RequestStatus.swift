//
//  RequestStatus.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 07/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

enum RequestStatusCode {
  case Ok
  case Error
}
class RequestStatus: NSObject {
  var statusCode : RequestStatusCode = .Ok
  var message : String?
}
