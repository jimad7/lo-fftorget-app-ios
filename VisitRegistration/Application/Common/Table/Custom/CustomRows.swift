//
//  CustomRows.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 06/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import UIKit
import Eureka

protocol FieldRowConformance2 : FormatterConformance2 {
  var textFieldPercentage : CGFloat? { get set }
  var placeholder : String? { get set }
  var placeholderColor : UIColor? { get set }
}

public protocol FormatterConformance2: class {
  var formatter: NSFormatter? { get set }
  var useFormatterDuringInput: Bool { get set }
}

/// A String valued row where the user can enter arbitrary text.
public final class AutoCompleteTextRow: _AutoCompleteTextRow, RowType {
  required public init(tag: String?) {
    super.init(tag: tag)
    onCellHighlight { cell, row  in
      let color = cell.textLabel?.textColor
      row.onCellUnHighlight { cell, _ in
        cell.textLabel?.textColor = color
      }
      cell.textLabel?.textColor = cell.tintColor
    }
  }
}

public class _AutoCompleteTextRow: FieldRow<String, AutoCompleteTextCell> {
  public required init(tag: String?) {
    super.init(tag: tag)
  }
}