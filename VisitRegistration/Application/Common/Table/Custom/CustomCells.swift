//  CustomCells.swift
//  Eureka ( https://github.com/xmartlabs/Eureka )
//
//  Copyright (c) 2015 Xmartlabs ( http://xmartlabs.com )
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation
import UIKit
import MapKit
import Eureka
import AutocompleteField

//MARK: AutoCompleteFieldCell
public class _AutoCompleteFieldCell<T where T: Equatable, T: InputTypeInitiable> : Cell<T>, UITextFieldDelegate, TextFieldCell {
  lazy public var textField : UITextField = {
    //var textField = AutocompleteField(frame: CGRectMake(40, 10, 200, 40), suggestions: self.suggestions )
    //var textField = AutoCompleteTextField(frame: CGRectMake(40, 10, 200, 40))
    var textField = MPGTextField_Swift(frame: CGRectMake(40, 10, 200, 40))
    textField.translatesAutoresizingMaskIntoConstraints = false
    return textField
  }()
  
  public var suggestion = ""
  
  public var suggestions : [String] = [""]
  
  public var titleLabel : UILabel? {
    textLabel?.translatesAutoresizingMaskIntoConstraints = false
    textLabel?.setContentHuggingPriority(500, forAxis: .Horizontal)
    textLabel?.setContentCompressionResistancePriority(1000, forAxis: .Horizontal)
    return textLabel
  }
  
  private var dynamicConstraints = [NSLayoutConstraint]()
  
  public required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  deinit {
    titleLabel?.removeObserver(self, forKeyPath: "text")
    imageView?.removeObserver(self, forKeyPath: "image")
  }
  
  public override func setup() {
    super.setup()
    selectionStyle = .None
    contentView.addSubview(titleLabel!)
    contentView.addSubview(textField)
    
    titleLabel?.addObserver(self, forKeyPath: "text", options: NSKeyValueObservingOptions.Old.union(.New), context: nil)
    imageView?.addObserver(self, forKeyPath: "image", options: NSKeyValueObservingOptions.Old.union(.New), context: nil)
    textField.addTarget(self, action: #selector(_AutoCompleteFieldCell.textFieldDidChange(_:)), forControlEvents: .EditingChanged)
    
  }
  
  public override func update() {
    super.update()
    detailTextLabel?.text = nil
    if let title = row.title {
      //textField.textAlignment = title.isEmpty ? .Left : .Right
      textField.textAlignment = .Left
      textField.clearButtonMode = title.isEmpty ? .WhileEditing : .Never
    }
    else{
      textField.textAlignment =  .Left
      textField.clearButtonMode =  .WhileEditing
    }
    textField.delegate = self
    //textField.text = row.displayValueFor?(row.value)
    textField.enabled = !row.isDisabled
    textField.textColor = row.isDisabled ? .grayColor() : .blackColor()
    textField.font = .preferredFontForTextStyle(UIFontTextStyleBody)
    if let placeholder = (row as? FieldRowConformance2)?.placeholder {
      if let color = (row as? FieldRowConformance2)?.placeholderColor {
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSForegroundColorAttributeName: color])
      }
      else{
        textField.placeholder = (row as? FieldRowConformance2)?.placeholder
      }
    }
  }
  
  public override func cellCanBecomeFirstResponder() -> Bool {
    return !row.isDisabled && textField.canBecomeFirstResponder()
  }
  
  public override func cellBecomeFirstResponder(direction: Direction) -> Bool {
    return textField.becomeFirstResponder()
  }
  
  public override func cellResignFirstResponder() -> Bool {
    return textField.resignFirstResponder()
  }
  
  public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
    if let obj = object, let keyPathValue = keyPath, let changeType = change?[NSKeyValueChangeKindKey] where ((obj === titleLabel && keyPathValue == "text") || (obj === imageView && keyPathValue == "image")) && changeType.unsignedLongValue == NSKeyValueChange.Setting.rawValue {
      setNeedsUpdateConstraints()
      updateConstraintsIfNeeded()
    }
  }
  
  // Mark: Helpers
  
  public override func updateConstraints(){
    contentView.removeConstraints(dynamicConstraints)
    dynamicConstraints = []
    var views : [String: AnyObject] =  ["textField": textField]
    dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|-11-[textField]-11-|", options: .AlignAllBaseline, metrics: nil, views: ["textField": textField])
    
    if let label = titleLabel, let text = label.text where !text.isEmpty {
      dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|-11-[titleLabel]-11-|", options: .AlignAllBaseline, metrics: nil, views: ["titleLabel": label])
      dynamicConstraints.append(NSLayoutConstraint(item: label, attribute: .CenterY, relatedBy: .Equal, toItem: textField, attribute: .CenterY, multiplier: 1, constant: 0))
    }
    if let imageView = imageView, let _ = imageView.image {
      views["imageView"] = imageView
      if let titleLabel = titleLabel, text = titleLabel.text where !text.isEmpty {
        views["label"] = titleLabel
        dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:[imageView]-[label]-[textField]-|", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        dynamicConstraints.append(NSLayoutConstraint(item: textField, attribute: .Width, relatedBy: (row as? FieldRowConformance2)?.textFieldPercentage != nil ? .Equal : .GreaterThanOrEqual, toItem: contentView, attribute: .Width, multiplier: (row as? FieldRowConformance2)?.textFieldPercentage ?? 0.3, constant: 0.0))
      }
      else{
        dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:[imageView]-[textField]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
      }
    }
    else{
      if let titleLabel = titleLabel, let text = titleLabel.text where !text.isEmpty {
        views["label"] = titleLabel
        dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:|-[label]-[textField]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        dynamicConstraints.append(NSLayoutConstraint(item: textField, attribute: .Width, relatedBy: (row as? FieldRowConformance2)?.textFieldPercentage != nil ? .Equal : .GreaterThanOrEqual, toItem: contentView, attribute: .Width, multiplier: (row as? FieldRowConformance2)?.textFieldPercentage ?? 0.3, constant: 0.0))
      }
      else{
        dynamicConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:|-[textField]-|", options: .AlignAllLeft, metrics: nil, views: views)
      }
    }
    contentView.addConstraints(dynamicConstraints)
    super.updateConstraints()
  }
  
  public func textFieldDidChange(textField : UITextField){
    guard let textValue = textField.text else {
      row.value = nil
      return
    }
    if let fieldRow = row as? FieldRowConformance2, let formatter = fieldRow.formatter where fieldRow.useFormatterDuringInput {
      let value: AutoreleasingUnsafeMutablePointer<AnyObject?> = AutoreleasingUnsafeMutablePointer<AnyObject?>.init(UnsafeMutablePointer<T>.alloc(1))
      let errorDesc: AutoreleasingUnsafeMutablePointer<NSString?> = nil
      if formatter.getObjectValue(value, forString: textValue, errorDescription: errorDesc) {
        row.value = value.memory as? T
        if var selStartPos = textField.selectedTextRange?.start {
          let oldVal = textField.text
          textField.text = row.displayValueFor?(row.value)
          if let f = formatter as? FormatterProtocol {
            selStartPos = f.getNewPosition(forPosition: selStartPos, inTextInput: textField, oldValue: oldVal, newValue: textField.text)
          }
          textField.selectedTextRange = textField.textRangeFromPosition(selStartPos, toPosition: selStartPos)
        }
        return
      }
    }
    guard !textValue.isEmpty else {
      row.value = nil
      return
    }
    guard let newValue = T.init(string: textValue) else {
      row.updateCell()
      return
    }
    row.value = newValue
  }
  
  //MARK: TextFieldDelegate
  
  public func textFieldDidBeginEditing(textField: UITextField) {
    formViewController()?.beginEditing(self)
    formViewController()?.textInputDidBeginEditing(textField, cell: self)
    if let fieldRowConformance = (row as? FieldRowConformance2), let _ = fieldRowConformance.formatter where !fieldRowConformance.useFormatterDuringInput {
      textField.text = row.displayValueFor?(row.value)
    }
  }
  
  public func textFieldDidEndEditing(textField: UITextField) {
    formViewController()?.endEditing(self)
    formViewController()?.textInputDidEndEditing(textField, cell: self)
    textFieldDidChange(textField)
    if let fieldRowConformance = (row as? FieldRowConformance2), let _ = fieldRowConformance.formatter where !fieldRowConformance.useFormatterDuringInput {
      textField.text = row.displayValueFor?(row.value)
    }
  }
  
  public func textFieldShouldReturn(textField: UITextField) -> Bool {
//    if let suggestion = (textField as! AutocompleteField).suggestion where !suggestion.isEmpty {
//      textField.text = suggestion
//    }
    
    return formViewController()?.textInputShouldReturn(textField, cell: self) ?? true
  }
  
  public func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    return formViewController()?.textInput(textField, shouldChangeCharactersInRange:range, replacementString:string, cell: self) ?? true
  }
  
  public func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    return formViewController()?.textInputShouldBeginEditing(textField, cell: self) ?? true
  }
  
  public func textFieldShouldClear(textField: UITextField) -> Bool {
    return formViewController()?.textInputShouldClear(textField, cell: self) ?? true
  }
  
  public func textFieldShouldEndEditing(textField: UITextField) -> Bool {
    return formViewController()?.textInputShouldEndEditing(textField, cell: self) ?? true
  }
  
  
}

//MARK: AutoCompleteTextCell

public class AutoCompleteTextCell : _AutoCompleteFieldCell<String>, CellType {
  
  required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  public override func setup() {
    super.setup()
    textField.autocorrectionType = .No
    textField.autocapitalizationType = .Sentences
    textField.keyboardType = .Default
  }
}
