//
//  VisitContainerViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 05/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import XLPagerTabStrip

class VisitContainerViewController: SegmentedPagerTabStripViewController {
  
  var isReload = false
  var visitId :Int = 0 //can be assigned value from MainViewController tableview row
  var visitType: String?
  
  @IBOutlet var menuButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      //menuButton.tintColor = UIColor.blackColor()
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    // change segmented style
    settings.style.segmentedControlColor = UIColor.lightGrayColor()

  }
  
  // MARK: - PagerTabStripDataSource
  override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
    if visitId > 0 && visitType != nil {
      if visitType == VisitType.Single.rawValue {
        return [SingleVisitViewController(id: visitId)]
      } else {
        return [GroupVisitViewController(id: visitId)]
      }
    }
    let child_1 = SingleVisitViewController(id: visitId)
    let child_2 = GroupVisitViewController(id: visitId)

    
    guard isReload else {
      return [child_1, child_2]
    }
    
    let childViewControllers = [child_1, child_2]
    return childViewControllers
  }

  @IBAction func closeAction(sender: UIBarButtonItem) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func reloadTapped(sender: UIBarButtonItem) {
    isReload = true
    pagerBehaviour = .Common(skipIntermediateViewControllers: true)
    pagerBehaviour.skipIntermediateViewControllers
        reloadPagerTabStripView()
  }
}