//
//  GroupVisitViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 05/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka
import XLPagerTabStrip
import AutocompleteField

class GroupVisitViewController : FormViewController, IndicatorInfoProvider, MPGTextFieldDelegate {
  var itemInfo = IndicatorInfo(title: NSLocalizedString("Gruppsamtal", comment: "Titel på gruppsamtalet"))
  var sampleData = [Dictionary<String, AnyObject>]()
  var site : String?
  var worksite : String?
  var lastEmployer:Worksite?
  var selectedUnion1 : DbUnion = DbUnion()
  var selectedSection1 : DbSection = DbSection()
  var selectedUnion2: DbUnion = DbUnion()
  var selectedSection2: DbSection = DbSection()
  var selectedUnion3: DbUnion = DbUnion()
  var selectedSection3: DbSection = DbSection()
  var selectedUnion4: DbUnion = DbUnion()
  var selectedSection4: DbSection = DbSection()
  var selectedUnion5: DbUnion = DbUnion()
  var selectedSection5: DbSection = DbSection()
  var group1: String? = ""
  var group2: String? = ""
  var group3: String? = ""
  var group4: String? = ""
  var group5: String? = ""
  var numberOfGroups: Int = 1
  var addressInfo : [String] = []
  var updateStreetAddress : Bool = false
  var updateZipCode : Bool = false
  var updateCity : Bool = false
  var visit:Visit?
  var visitId : Int = 0
  var visitContent: [String : AnyObject]?
  
  convenience init(id: Int) {
    self.init(nibName:nil, bundle:nil)
    visitId = id
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    //Helper values for PickerInlineRows below
    let allWorksites = StoreManager.sharedInstance.allWorksites()
    let allUnions = StoreManager.sharedInstance.allUnions()
    let allDistricts = StoreManager.sharedInstance.allDistricts()
    let allProjects = StoreManager.sharedInstance.allProjects()
    let profile = StoreManager.sharedInstance.profile(ProfileData.id)!

    var updateSection1 : Bool = false
    var updateSection2 : Bool = false
    var updateSection3 : Bool = false
    var updateSection4 : Bool = false
    var updateSection5 : Bool = false
    
    //data for autocomplete field
    for worksite in allWorksites {
      let info: [String] = [worksite.unid!,worksite.streetaddress!,worksite.zipcode!,worksite.city!]
      let dictionary = ["DisplayText":worksite.unid!.componentsSeparatedByString("-")[0],"DisplaySubText":worksite.name!,"CustomObject":info]
      sampleData.append(dictionary as! Dictionary<String, AnyObject>)
    }
    
    if visitId != 0{
      self.numberOfGroups = 1 //Only one group is displayed when editing visits
      visit = StoreManager.sharedInstance.visit(visitId)
      if let content = visit!.visitcontent {
        visitContent = NSKeyedUnarchiver.unarchiveObjectWithData(content) as? [String : AnyObject]
        selectedUnion1 = visitContent!["user_union1"] as! DbUnion
        selectedSection1 = visitContent!["user_section1"] as! DbSection
      } else {
        visitContent = [String: AnyObject]()
      }
    } else {
      selectedUnion1 = DbUnion(unid: profile.dbunionid!, name: profile.dbunion!)
      selectedSection1 = DbSection(unid: profile.dbsectionid!, name: profile.dbsection!)
    }
    
    var allSettings = StoreManager.sharedInstance.allSettings()
    if allSettings.count > 0 && allSettings[0].employer != "" {
        for worksite in allWorksites {
            let unid:String = worksite.unid!.componentsSeparatedByString("-")[0]
            if(unid == allSettings[0].employer as String) {
                lastEmployer = worksite
                break
            }
        }
    }
    
    // MARK: - Employer section
    form
      +++ Section(NSLocalizedString("Arbetsgivare", comment: "Titel på sektionen"))
      <<< AutoCompleteTextRow("employer") {
        $0.title = NSLocalizedString("Arbetsgivare*", comment: "Namnet på arbetsgivaren")
        //Needed to get values in self.form.values()
        if visit != nil {
          $0.value = visit!.employer
        } else if allSettings.count > 0 {
            $0.value = allSettings[0].employer
        }
        
        }.cellSetup { cell, row in
          (cell.textField as! MPGTextField_Swift).mDelegate = self
          (cell.textField as! MPGTextField_Swift).popoverBackgroundColor = UIColor.lightGrayColor()
          if self.visit != nil {
            cell.textField.text = self.visit!.employer
          } else if allSettings.count > 0 {
            cell.textField.text = allSettings[0].employer
          }
      }
      <<< NameRow("site") {(row : NameRow) -> Void in
        row.title = NSLocalizedString("Arbetsplats", comment: "Var den intervjuade arbetar")
        if visit != nil{
          row.value = visitContent!["site"] as? String
        }else{
            row.value = lastEmployer?.name
        }
        
        row.displayValueFor = {
          if let site = self.site {
            row.value = site
          }
          
          guard let value = $0 else{
            return nil
          }
          return value
        }
      }
      <<< NameRow("streetaddress") {
        $0.title = NSLocalizedString("Gatuadress", comment: "Arbetsplatsens adress")
        if  visit != nil{
          $0.value = visitContent!["streetaddress"] as? String
        }else{
            $0.value = lastEmployer?.streetaddress
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateStreetAddress {
            self.updateStreetAddress = false
            row.value = self.addressInfo[1]
            row.updateCell()
          }
      }
      <<< NameRow("zipcode") {
        $0.title = NSLocalizedString("Postnummer", comment: "Arbetsplatsens postnummer")
        if visit != nil{
          $0.value = visitContent!["zipcode"] as? String
        }
        else{
            $0.value = lastEmployer?.zipcode
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateZipCode {
            self.updateZipCode = false
            row.value = self.addressInfo[2]
            row.updateCell()
          }
      }
      <<< TextRow("city") {
        $0.title = NSLocalizedString("Ort", comment: "Arbetsplatsens ort")
        if visit != nil{
          $0.value = visitContent!["city"] as? String
        }
        else{
            $0.value = lastEmployer?.city
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateCity {
            self.updateCity = false
            row.value = self.addressInfo[3]
            row.updateCell()
          }
      }
      
      // MARK: - Information section
      +++ Section(NSLocalizedString("Uppgifter", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< DateInlineRow("interviewdate") {
        $0.title = NSLocalizedString("Datum*", comment: "Arbetstagarens datum")
        if visit != nil{
          $0.value = visit?.interviewdate
        } else {
          $0.value = NSDate()
        }
      }
      <<< PickerInlineRow<Project>("user_project") { (row : PickerInlineRow<Project>) -> Void in
        row.title = NSLocalizedString("Tillhör projekt", comment: "Den inloggade informatörens projekt")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as Project).name
        }
        if !allProjects.isEmpty {
          row.options = allProjects
          if allProjects.count == 1 {
            row.value = row.options[0]
          } else {
            let index = row.options.indexOf({$0.unid == "0"}) ?? 0
            row.value = row.options[index]
          }
        }
        if visit != nil{
          row.value = (visitContent!["user_project"] as? Project)
        }
      }
      //MARK: - Group1
      +++ Section(NSLocalizedString("Grupp 1", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< PickerInlineRow<DbUnion>("user_union1") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          //selectedUnion1 is either based on previously saved value, or the profile default value
          row.options = allUnions
          let index = row.options.indexOf({$0.unid == selectedUnion1.unid}) ?? 0
          row.value = row.options[index]
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.selectedUnion1 = row.value!
            self!.selectedSection1 = DbSection()
            updateSection1 = true
            self?.form.rowByTag("user_section1")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section1") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "Den inloggade informatörens avdelning")
        
        row.displayValueFor = {
          guard let value = $0 where !updateSection1 else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          //selectedSection1 is either based on previously saved value, or the profile default value
          self.infoForSection(row, unionid: self.selectedUnion1.unid, sectionid: self.selectedSection1.unid)
        }
        row.cellUpdate {cell, row in
          if updateSection1 {
            updateSection1 = false
            self.infoForSection(row, unionid: self.selectedUnion1.unid, sectionid: self.selectedSection1.unid)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
            self.initializeQueryGroups(self.selectedUnion1.name!, sectionName: (cell.detailTextLabel?.text)!, groupNo: "1")
          }
        }
        row.onChange { [weak self] row in
          if row.value != "" {
            self?.selectedSection1 = row.value!
            self?.initializeQueryGroups((self?.selectedUnion1.name)!, sectionName: self?.selectedSection1.name, groupNo: "1")
          }
        }
      }
      <<< PickerInlineRow<District>("user_district1") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "Alla LO:s distrikt")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
          let index = row.options.indexOf({$0.unid == profile.districtid}) ?? 0
          row.value = row.options[index]
        }
        if visit != nil{
          row.value = (visitContent!["user_district1"] as? District)
        }
      }
      <<< IntRow("group_membership_status_yes1"){
        $0.title = NSLocalizedString("Antal medlemmar", comment: "Medlemsstatus för de intervjuade")
        if visit != nil {
          $0.value = visitContent!["group_membership_status_yes1"] as? Int
        }
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_no1"){
        $0.title = NSLocalizedString("Antal ej medlemmar", comment: "Medlemsstatus för de intervjuade")
        if visit != nil {
          $0.value = visitContent!["group_membership_status_no1"] as? Int
        }
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_unknown1"){
        $0.title = NSLocalizedString("Antal okända", comment: "Medlemsstatus för de intervjuade")
        if visit != nil{
          $0.value = visitContent!["group_membership_status_unknown1"] as? Int
        }
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      //MARK: Group 2
      +++ Section(NSLocalizedString("Grupp 2", comment: "Titel på sektionen")) {
        $0.header?.height = {20}
        
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
      }
      <<< PickerInlineRow<DbUnion>("user_union2") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.selectedUnion2 = row.value!
            self!.selectedSection2 = DbSection()
            updateSection2 = true
            self?.form.rowByTag("user_section2")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section2") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "")
        row.displayValueFor = {
          guard let value = $0 where !updateSection2 else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          self.infoForSection(row, unionid: self.selectedUnion2.unid, sectionid: self.selectedSection2.unid)
        }
        row.cellUpdate {cell, row in
          if updateSection2 {
            updateSection2 = false
          self.infoForSection(row, unionid: self.selectedUnion2.unid, sectionid: self.selectedSection2.unid)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
            self.initializeQueryGroups(self.selectedUnion2.name!, sectionName: cell.detailTextLabel?.text, groupNo: "2")
          }
        }
        row.onChange { [weak self] row in
          if row.value != "" {
            self?.selectedSection2 = row.value!
            self?.initializeQueryGroups((self?.selectedUnion2.name)!, sectionName: self?.selectedSection2.name, groupNo: "2")
          }
        }
      }
      <<< PickerInlineRow<District>("user_district2") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
        }
      }
      <<< IntRow("group_membership_status_yes2"){
        $0.title = NSLocalizedString("Antal medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_no2"){
        $0.title = NSLocalizedString("Antal ej medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_unknown2"){
        $0.title = NSLocalizedString("Antal okända", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      //MARK: Group 3
      +++ Section(NSLocalizedString("Grupp 3", comment: "Titel på sektionen")) {
        $0.header?.height = {20}
        
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
      }
      <<< PickerInlineRow<DbUnion>("user_union3") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.selectedUnion3 = row.value!
            self!.selectedSection3 = DbSection()
            updateSection3 = true
            self?.form.rowByTag("user_section3")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section3") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "")
        row.displayValueFor = {
          guard let value = $0 where !updateSection3 else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          self.infoForSection(row, unionid: self.selectedUnion3.unid, sectionid: self.selectedSection3.unid)
        }
        row.cellUpdate {cell, row in
          if updateSection3 {
            updateSection3 = false
          self.infoForSection(row, unionid: self.selectedUnion3.unid, sectionid: self.selectedSection3.unid)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
            self.initializeQueryGroups(self.selectedUnion3.name!, sectionName: cell.detailTextLabel?.text, groupNo: "3")
          }
        }
        row.onChange { [weak self] row in
          if row.value != "" {
            self?.selectedSection3 = row.value!
            self?.initializeQueryGroups((self?.selectedUnion3.name)!, sectionName: self?.selectedSection3.name, groupNo: "3")
          }
        }
      }
      <<< PickerInlineRow<District>("user_district3") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
        }
      }
      <<< IntRow("group_membership_status_yes3"){
        $0.title = NSLocalizedString("Antal medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_no3"){
        $0.title = NSLocalizedString("Antal ej medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_unknown3"){
        $0.title = NSLocalizedString("Antal okända", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      
      +++ Section(NSLocalizedString("Grupp 4", comment: "Titel på sektionen")) {
        $0.header?.height = {20}
        
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
      }
      //MARK: Group 4
      <<< PickerInlineRow<DbUnion>("user_union4") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.selectedUnion4 = row.value!
            self!.selectedSection4 = DbSection()
            updateSection4 = true
            self?.form.rowByTag("user_section4")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section4") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "")
        row.displayValueFor = {
          guard let value = $0 where !updateSection4 else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          self.infoForSection(row, unionid: self.selectedUnion4.unid, sectionid: self.selectedSection4.unid)
        }
        row.cellUpdate {cell, row in
          if updateSection4 {
            updateSection4 = false
          self.infoForSection(row, unionid: self.selectedUnion4.unid, sectionid: self.selectedSection4.unid)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
            self.initializeQueryGroups(self.selectedUnion4.name!, sectionName: cell.detailTextLabel?.text, groupNo: "4")
          }
        }
        row.onChange { [weak self] row in
          if row.value != "" {
            self?.selectedSection4 = row.value!
            self?.initializeQueryGroups((self?.selectedUnion4.name)!, sectionName: self?.selectedSection4.name, groupNo: "4")
          }
        }
      }
      <<< PickerInlineRow<District>("user_district4") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
        }
      }
      <<< IntRow("group_membership_status_yes4"){
        $0.title = NSLocalizedString("Antal medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_no4"){
        $0.title = NSLocalizedString("Antal ej medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_unknown4"){
        $0.title = NSLocalizedString("Antal okända", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      //MARK: Group 5
      +++ Section(NSLocalizedString("Grupp 5", comment: "Titel på sektionen")) {
        $0.header?.height = {20}
        
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
      }
      <<< PickerInlineRow<DbUnion>("user_union5") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.selectedUnion5 = row.value!
            self!.selectedSection5 = DbSection()
            updateSection5 = true
            self?.form.rowByTag("user_section5")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section5") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "")
        row.displayValueFor = {
          guard let value = $0 where !updateSection5 else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          self.infoForSection(row, unionid: self.selectedUnion5.unid, sectionid: self.selectedSection5.unid)
        }
        row.cellUpdate {cell, row in
          if updateSection5 {
            updateSection5 = false
          self.infoForSection(row, unionid: self.selectedUnion5.unid, sectionid: self.selectedSection5.unid)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
            self.initializeQueryGroups(self.selectedUnion5.name!, sectionName: cell.detailTextLabel?.text, groupNo: "5")
          }
        }
        row.onChange { [weak self] row in
          if row.value != "" {
            self?.selectedSection5 = row.value!
            self?.initializeQueryGroups((self?.selectedUnion5.name)!, sectionName: self?.selectedSection5.name, groupNo: "5")
          }
        }
      }
      <<< PickerInlineRow<District>("user_district5") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
        }
      }
      <<< IntRow("group_membership_status_yes5"){
        $0.title = NSLocalizedString("Antal medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_no5"){
        $0.title = NSLocalizedString("Antal ej medlemmar", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      <<< IntRow("group_membership_status_unknown5"){
        $0.title = NSLocalizedString("Antal okända", comment: "Medlemsstatus för de intervjuade")
      }.cellSetup {cell, row in
          self.setMembershipNumberCell(cell)
      }
      
      +++ Section()
      
      //MARK: - Show/hide more group info
      <<< ButtonRow("add_section") {
        $0.title = "Lägg till fler"
        $0.value = String(self.numberOfGroups)
        $0.hidden = visitId != 0 ? true : false
        }.onCellSelection {  cell, row in
          if self.numberOfGroups == 5 {
            self.informUser("Det går inte att lägga till fler grupper. Skapa ett till samtal för att kunna ange fler grupper.")
            row.disabled = true
            row.evaluateDisabled()
          } else {
            self.numberOfGroups += 1
            row.value = String(self.numberOfGroups) //Affects groups section visibility
          }
          
          if self.numberOfGroups >= 2 {
            self.form.rowByTag("hide_section")?.hidden = false //Affects 'Ångra åtgärd' visibility
            self.form.rowByTag("hide_section")?.evaluateHidden()
          }
        }.cellUpdate {cell, row in
          if self.numberOfGroups < 5 && row.isDisabled {
            row.disabled = false
            row.evaluateDisabled()
          }
        }
      
      <<< ButtonRow("hide_section") {
        $0.title = "Ångra tillägg"
        $0.hidden = Condition.Function(["remove"]) { form in
          return self.numberOfGroups == 1
        }
        }.onCellSelection {  cell, row in
          self.numberOfGroups -= 1
          self.form.rowByTag("add_section")?.value = String(self.numberOfGroups) //Affects groups section visibility
          self.form.rowByTag("add_section")?.updateCell()
          
          if self.numberOfGroups == 1 {
            row.hidden = true
            row.evaluateHidden()
          }
      }
      
      // MARK: - Yes/No queries sections
      
      // MARK: Illness
      +++ Section(NSLocalizedString("Sjukdom", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_illness_label1"){
        $0.title = "Hur många har varit sjukskrivna längre än 14 dagar, och missat att ansöka om ersättning enligt kollektivavtal?"
        }.cellSetup { cell, row in
          self.setLabelCell(cell)
      }
      <<< IntRow("query_illness1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_illness1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_illness2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_illness3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_illness4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_illness5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Injury
      +++ Section(NSLocalizedString("Arbetsskada", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_injury_label1"){
        $0.title = "Hur många har skadats på eller av jobbet, och missat att anmäla skadan och ansöka om ersättning enligt kollektivavtal?"
        }.cellSetup { cell, row in
          self.setLabelCell(cell)
      }
      <<< IntRow("query_injury1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_injury1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_injury2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_injury3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_injury4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_injury5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Unemployment
      +++ Section(NSLocalizedString("Arbetslöshet", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_unemployment_label1"){
        $0.title = "Hur många har blivit/är arbetslösa, och missat ersättning enligt kollektivavtal?"
        }.cellSetup { cell, row in
          self.setMediumLabelCell(cell)
      }
      <<< IntRow("query_unemployment1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_unemployment1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_unemployment2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_unemployment3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_unemployment4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_unemployment5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Retirement
      +++ Section(NSLocalizedString("Ålderspension", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_retirement_label1"){
        $0.title = "Hur många har missat att göra aktiva val för sin avtalspension?"
        }.cellSetup { cell, row in
          self.setMediumLabelCell(cell)
      }
      <<< IntRow("query_retirement1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_retirement1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_retirement2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_retirement3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_retirement4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_retirement5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Retirement continued
      +++ Section(NSLocalizedString("Ålderspension forts.", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_retirement_continued_label1"){
        $0.title = "Hur många har missat att ansöka om ersättning från premiebefrielseförsäkringen?"
        }.cellSetup { cell, row in
          self.setMediumLabelCell(cell)
      }
      <<< IntRow("query_retirement_continued1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_retirement_continued1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_retirement_continued2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_retirement_continued3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_retirement_continued4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_retirement_continued5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Deceased
      +++ Section(NSLocalizedString("Dödsfall", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_death_label1"){
        $0.title = "Hur många har en anhörig som avlidit, och missat att ansöka om ersättning enligt kollektivavtal?"
        }.cellSetup { cell, row in
          self.setLabelCell(cell)
      }
      <<< IntRow("query_death1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_death1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_death2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_death3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_death4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_death5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Parent
      +++ Section(NSLocalizedString("Förälder", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_parenthood_label1"){
        $0.title = "Hur många har, efter den 1 januari 2014, varit föräldralediga med föräldrapenning, och missat att ansöka om ersättning enligt kollektivavtal?"
        }.cellSetup { cell, row in
          self.setLargeLabelCell(cell)
      }
      <<< IntRow("query_parenthood1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_parenthood1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_parenthood2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_parenthood3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_parenthood4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_parenthood5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
      //MARK: Membership
      +++ Section(NSLocalizedString("Medlemsförsäkringar", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< LabelRow("query_membership_label1"){
        $0.title = "Hur många kan tänka sig bli medlemmar?"
        }.cellSetup { cell, row in
          self.setSmallLabelCell(cell)
      }
      <<< IntRow("query_membership1"){
        $0.title = ""
        if visit != nil{
          $0.value = visitContent!["query_membership1"] as? Int
        }
      }.cellSetup { cell, row in
          self.setIntCell(cell)
      }.cellUpdate {cell, row in
          row.title = self.group1
      }
      <<< IntRow("query_membership2"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 1
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group2
      }
      <<< IntRow("query_membership3"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 2
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group3
      }
      <<< IntRow("query_membership4"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 3
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group4
      }
      <<< IntRow("query_membership5"){
        $0.title = ""
        $0.hidden = .Function(["add_section"], { form -> Bool in
          if let r:ButtonRow = form.rowByTag("add_section") {
            return Int(r.value!) <= 4
          }
          return true
        })
        }.cellSetup { cell, row in
          self.setIntCell(cell)
        }.cellUpdate {cell, row in
          row.title = self.group5
      }
      
        +++ Section(NSLocalizedString("Övriga kommentarer", comment: "Titel på sektionen")) { section in
            section.header?.height = {20}
        }
        
      <<< TextAreaRow("comments"){
        $0.placeholder = NSLocalizedString("", comment: "Titel på fältet")
        if visit != nil{
          $0.value = visitContent!["comments"] as? String
        }
        }.cellSetup { cell, row in
          cell.placeholderLabel.textColor = UIColor.blackColor()
      }
      
      +++ Section() { section in
        section.header?.height = {20}
      }
      //MARK: - Save section

      <<< ButtonRow("save_group_visit") {
        $0.title = "Spara"
        }.onCellSelection {  cell, row in
          var values = self.form.values()
          let errors = self.validateForm(values)
          
          if errors.isEmpty {
            if self.visit != nil{
              values["_id"] = self.visit?._id
              values["unid"] = self.visit?.unid ?? ""
              values["sent"] = self.visit?.sent
            } else {
              values["_id"] = 0
              values["unid"] = ""
            }
            values["visittype"] = VisitType.Group.rawValue
            values["interviewee"] = ""
            
            //Set quantity fields to zero if missing
            for i in 1...self.numberOfGroups {
              if values["group_membership_status_yes\(i)"]! == nil {
                values["group_membership_status_yes\(i)"] = 0
              }
              if values["group_membership_status_no\(i)"]! == nil {
                values["group_membership_status_no\(i)"] = 0
              }
              if values["group_membership_status_unknown\(i)"]! == nil {
                values["group_membership_status_unknown\(i)"] = 0
              }
              if values["query_illness\(i)"]! == nil {
                values["query_illness\(i)"] = 0
              }
              if values["query_injury\(i)"]! == nil {
                values["query_injury\(i)"] = 0
              }
              if values["query_unemployment\(i)"]! == nil {
                values["query_unemployment\(i)"] = 0
              }
              if values["query_retirement\(i)"]! == nil {
                values["query_retirement\(i)"] = 0
              }
              if values["query_retirement_continued\(i)"]! == nil {
                values["query_retirement_continued\(i)"] = 0
              }
              if values["query_death\(i)"]! == nil {
                values["query_death\(i)"] = 0
              }
              if values["query_parenthood\(i)"]! == nil {
                values["query_parenthood\(i)"] = 0
              }
              if values["query_membership\(i)"]! == nil {
                values["query_membership\(i)"] = 0
              }
            }
            ////////////////////////////////////////
            
            //update employer (possibly add settings)
            var allSettings = StoreManager.sharedInstance.allSettings()
            let hasSettings = allSettings.count > 0
            var settings = [String:Any?]()
            settings["_id"] = hasSettings ? allSettings[0]._id : 0
            settings["synchronization"] = hasSettings ? allSettings[0].synchronization : true
            settings["no_of_visits"] = hasSettings ? allSettings[0].numberOfVisits : Config.numberOfVisits
            settings["employer"] = (values["employer"] as? String)!
            StoreManager.sharedInstance.addOrUpdateSettingsLocally(settings)
            /////////////////////////////////////////
            
            //A new group visit may exist of 1-5 groups in the UI and the HTTP request
            //whereas an existing group visit is always just 1 group in the UI and the HTTP request
            //(since they are always stored one by one in the back-end, with just a 'group' flag)
            if self.visit != nil {
              self.processExistingGroupVisit(values)
            } else {
              self.processNewGroupVisits(values)
            }
            
          } else {
            self.showValidationErrors(errors)
          }
      }
      +++ Section()
  }
  
  func processNewGroupVisits(values: [String: Any?]) {
    
    let visits = StoreManager.sharedInstance.addGroupVisitsLocally(values, numberOfGroups: self.numberOfGroups)

    if StoreManager.sharedInstance.synchronizeAutomatically() {
      let firstVisit = visits[0]
      let dbDictionary:[String:AnyObject] = NSKeyedUnarchiver.unarchiveObjectWithData(firstVisit.visitcontent!) as! [String : AnyObject]
      var dict : [String: AnyObject] = [String: AnyObject]()
      dict["id"] = ProfileData.id
      dict["informator"] = ProfileData.fullname
      let union = values["user_union1"] as! DbUnion
      dict["informatorforbund"] = union.name
      dict["informatorforbundid"] = union.unid
      let district = values["user_district1"] as! District
      dict["informatordistrikt"] = district.name
      dict["informatordistriktid"] = district.unid
      let dbsection = values["user_section1"] as! DbSection
      dict["informatoravd"] = dbsection.name
      dict["informatoravdid"] = dbsection.unid
      dict["motesid"] = ""
      dict["Type"] = VisitType.Group.rawValue
      dict["Arbetsgivare"] = firstVisit.employer
      dict["Arbetsplats"] = dbDictionary["site"] ?? ""
      dict["Gatuadress"] = dbDictionary["streetaddress"] ?? ""
      dict["Postnummer"] = dbDictionary["zipcode"]  ?? ""
      dict["Ort"] = dbDictionary["city"] ?? ""
      dict["Namn"] = firstVisit.interviewee
      dict["epost"] = ""
      dict["phone"] = ""
      dict["Datum"] = firstVisit.interviewdate!.stringFromDateWithFormat(Config.dateFormat)
      dict["projekt"] = firstVisit.projectid
      dict["medlem"] = ""
      dict["ags"] = "0"
      dict["tfa"] = "0"
      dict["agb"] = "0"
      dict["avtalspension"] = "0"
      dict["premiebefrielse"] = "0"
      dict["tglagl"] = "0"
      dict["fpt"] = "0"
      dict["intresseradAvMedlemsskap"] = "0"
      
      for groupNo in 1...self.numberOfGroups {
        let groupUnion = values["user_union\(groupNo)"] as! DbUnion
        let groupSection = values["user_section\(groupNo)"] as! DbSection
        let groupDistrict = values["user_district\(groupNo)"] as! District
        dict["Forbund\(groupNo - 1)"] = groupUnion.unid
        dict["Avdelning\(groupNo - 1)"] = groupSection.unid
        dict["Distrikt\(groupNo - 1)"] = groupDistrict.unid
        
        dict["antalMedlemmar\(groupNo - 1)"] = values["group_membership_status_yes\(groupNo)"] as! Int
        dict["antalEjMedlemmar\(groupNo - 1)"] = values["group_membership_status_no\(groupNo)"] as! Int
        dict["antalOkanda\(groupNo - 1)"] = values["group_membership_status_unknown\(groupNo)"] as! Int
        
        dict["ags\(groupNo - 1)"] = values["query_illness\(groupNo)"] as! Int
        dict["tfa\(groupNo - 1)"] = values["query_injury\(groupNo)"] as! Int
        dict["agb\(groupNo - 1)"] = values["query_unemployment\(groupNo)"] as! Int
        
        dict["avtalspension\(groupNo - 1)"] = values["query_retirement\(groupNo)"] as! Int
        dict["premiebefrielse\(groupNo - 1)"] = values["query_retirement_continued\(groupNo)"] as! Int
        dict["tglagl\(groupNo - 1)"] = values["query_death\(groupNo)"] as! Int
        dict["fpt\(groupNo - 1)"] = values["query_parenthood\(groupNo)"] as! Int
        dict["intresseradAvMedlemsskap\(groupNo - 1)"] = values["query_membership\(groupNo)"] as! Int
      }
      
      dict["Kommentar"] = dbDictionary["comments"] ?? ""
      let ids = visits.map{$0._id}
      self.tryToSendVisit(dict, ids: ids)
    } else {
        self.navigate()
    }
  }
  
  func processExistingGroupVisit(values: [String: Any?]) {
    let id = StoreManager.sharedInstance.addOrUpdateVisitLocally(values)
    
    if StoreManager.sharedInstance.synchronizeAutomatically() {
        let item = StoreManager.sharedInstance.visit(id)
        let ids : [Int] = [(item?._id)!]
        self.tryToSendVisit(item!.dataToSynchronize(), ids: ids)
    } else {
        self.navigate()
    }
  }
  
  
  func validateForm(values: [String : Any?]) -> [String] {
    var errors: [String] = []
    if values["employer"]! == nil || (values["employer"] as! String).isEmpty {
      errors.append("Arbetsgivare")
    }
    if values["interviewdate"]! == nil {
      errors.append("Datum")
    }
    for i in 1...self.numberOfGroups {
      if values["user_union\(i)"]! == nil {
        errors.append("Förbund\(i)")
      }
      if values["user_section\(i)"]! == nil {
        errors.append("Avdelning\(i)")
      }
      if values["user_district\(i)"]! == nil {
        errors.append("Distrikt\(i)")
      }
    }
    
    return errors
  }
  
  func showValidationErrors(errors: [String]) {
    let message = "Följande fält måste fyllas i: \(errors.joinWithSeparator(", "))"
    let alertController = UIAlertController(title: "Valideringsfel", message: message, preferredStyle: .Alert)
    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertController.addAction(defaultAction)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  //MARK: - Helper functions for rows
  func initializeQueryGroups(unionName: String, sectionName: String?, groupNo: String) {
    switch groupNo {
    case "1":
      self.group1 = unionName + " : " + (sectionName ?? "")
    case "2":
      self.group2 = unionName + " : " + (sectionName ?? "")
    case "3":
      self.group3 = unionName + " : " + (sectionName ?? "")
    case "4":
      self.group4 = unionName + " : " + (sectionName ?? "")
    case "5":
      self.group5 = unionName + " : " + (sectionName ?? "")
    default:
      self.group1 = unionName + " : " + (sectionName ?? "")
    }
    
    self.form.rowByTag("query_illness\(groupNo)")?.updateCell()
    self.form.rowByTag("query_injury\(groupNo)")?.updateCell()
    self.form.rowByTag("query_unemployment\(groupNo)")?.updateCell()
    self.form.rowByTag("query_retirement\(groupNo)")?.updateCell()
    self.form.rowByTag("query_retirement_continued\(groupNo)")?.updateCell()
    self.form.rowByTag("query_death\(groupNo)")?.updateCell()
    self.form.rowByTag("query_parenthood\(groupNo)")?.updateCell()
    self.form.rowByTag("query_membership\(groupNo)")?.updateCell()
  }
  
  func setLabelCell(cell: LabelCell) {
    cell.height = {90}
    cell.textLabel!.numberOfLines = 0
    cell.textLabel?.preferredMaxLayoutWidth = (cell.textLabel?.bounds.size.width)!
  }
  func setSmallLabelCell(cell: LabelCell) {
    cell.height = {50}
    cell.textLabel!.numberOfLines = 0
    cell.textLabel?.preferredMaxLayoutWidth = (cell.textLabel?.bounds.size.width)!
  }
  func setMediumLabelCell(cell: LabelCell) {
    cell.height = {70}
    cell.textLabel!.numberOfLines = 0
    cell.textLabel?.preferredMaxLayoutWidth = (cell.textLabel?.bounds.size.width)!
  }
  func setLargeLabelCell(cell: LabelCell) {
    cell.height = {110}
    cell.textLabel!.numberOfLines = 0
    cell.textLabel?.preferredMaxLayoutWidth = (cell.textLabel?.bounds.size.width)!
  }
  func setIntCell(cell: IntCell) {
    cell.height = {80}
    cell.titleLabel?.numberOfLines = 0
    cell.titleLabel?.preferredMaxLayoutWidth = (cell.titleLabel?.bounds.size.width)!
    cell.textField.attributedPlaceholder = NSAttributedString(string: "__")
  }
  func setMembershipNumberCell(cell: IntCell) {
    cell.textField.attributedPlaceholder = NSAttributedString(string: "__")
  }
  // MARK: - Helper functions for inline pickers
  func infoForSection(row: PickerInlineRow<DbSection>, unionid: String?, sectionid: String?) {
    if unionid != nil {
      let sections = StoreManager.sharedInstance.sectionsForUnion(unionid!)
      if !sections.isEmpty {
        row.options = sections
        //if sectionid != nil {
          let index = row.options.indexOf({$0.unid == sectionid}) ?? 0
          row.value = row.options[index]
        //}
      }
    }

  }
  
  // MARK: - Synchronize data
  func tryToSendVisit(dict: [String: AnyObject]?, ids: [Int]) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true

    NetworkManager.sharedInstance.sendData("setMote", parameters: dict!, completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if let _ = JSON["id"]! {
          StoreManager.sharedInstance.updateVisits(JSON, ids: ids)
          self.navigate()
        } else {
          status.message = NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren")
          self.informUserAndNavigate(status)
        }
      case .Failure(let error):
        if let error = error as NSError? {
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
          } else {
            status.message = error.localizedDescription
          }
          self.informUserAndNavigate(status)
        }
      }
    })
  }
  
  func navigate() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)    
    let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
    if self.revealViewController() != nil {
      self.revealViewController().pushFrontViewController(vc, animated: true)
    } else if self.parentViewController?.revealViewController() != nil {
      self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
    }
  }
  
  func informUserAndNavigate(status: RequestStatus) {
    let alertController = UIAlertController(title: "Information", message: status.message, preferredStyle: .Alert)
    
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
      UIAlertAction in
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
      if self.revealViewController() != nil {
        self.revealViewController().pushFrontViewController(vc, animated: true)
      } else if self.parentViewController?.revealViewController() != nil {
        self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
      }
    }
    
    // Add the actions
    alertController.addAction(okAction)
    self.presentViewController(alertController, animated: true, completion: nil)
    
  }
  
  // MARK: - Inform function
  func informUser(message: String) {
    let alertController = UIAlertController(title: "Information", message: message, preferredStyle: .Alert)
    
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) {
      UIAlertAction in
    }
    
    // Add the actions
    alertController.addAction(okAction)
    self.presentViewController(alertController, animated: true, completion: nil)
    
  }
  
  // MARK: - Tab title
  
  func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return itemInfo
  }
  
  // MARK: - Autocomplete field implementations
  func dataForPopoverInTextField(textfield: MPGTextField_Swift) -> [Dictionary<String, AnyObject>]
  {
    return sampleData
  }
  
  func textFieldShouldSelect(textField: MPGTextField_Swift) -> Bool{
    return true
  }
  
  func textFieldDidEndEditing(textField: MPGTextField_Swift, withSelection data: Dictionary<String,AnyObject>){
    self.site = data["DisplaySubText"] as? String
    //self.worksite = data["CustomObject"] as? String
    addressInfo = data["CustomObject"] as! [String]
    self.form.rowByTag("site")?.updateCell()
    self.updateStreetAddress = true
    self.updateZipCode = true
    self.updateCity = true
    self.form.rowByTag("streetaddress")?.updateCell()
    self.form.rowByTag("zipcode")?.updateCell()
    self.form.rowByTag("city")?.updateCell()
  }
}
