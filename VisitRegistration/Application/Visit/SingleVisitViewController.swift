//
//  SingleVisitViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 05/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka
import XLPagerTabStrip
import AutocompleteField

class SingleVisitViewController : FormViewController, IndicatorInfoProvider, MPGTextFieldDelegate {
  var itemInfo = IndicatorInfo(title: NSLocalizedString("Enskilt samtal", comment: "Titel på det enskilda samtalet"))
  var sampleData = [Dictionary<String, AnyObject>]()
  var site : String?
  var worksite : String?
  var userUnionId = ""//ProfileData.dbunion.unid
  var userSectionId = ""//ProfileData.dbsection.unid
  var addressInfo : [String] = []
  var updateStreetAddress : Bool = false
  var updateZipCode : Bool = false
  var updateCity : Bool = false
  var lastEmployer:Worksite?
    
  var visit:Visit?
  var visitId :Int = 0
  var visitContent: [String : AnyObject]?
  
  convenience init(id: Int) {
    self.init(nibName:nil, bundle:nil)
    visitId = id
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Helper values for PickerInlineRows below
    let allWorksites = StoreManager.sharedInstance.allWorksites()
    let allUnions = StoreManager.sharedInstance.allUnions()
    let allDistricts = StoreManager.sharedInstance.allDistricts()
    let allProjects = StoreManager.sharedInstance.allProjects()
    let profile = StoreManager.sharedInstance.profile(ProfileData.id)
    var updateSection : Bool = false
    
    //data for autocomplete field
    for worksite in allWorksites {
      let info: [String] = [worksite.unid!,worksite.streetaddress!,worksite.zipcode!,worksite.city!]
      let dictionary = ["DisplayText":worksite.unid!.componentsSeparatedByString("-")[0],"DisplaySubText":worksite.name!,"CustomObject":info]
      sampleData.append(dictionary as! Dictionary<String, AnyObject>)
    }
    
    //a previously saved visit should have values
    if visitId != 0{
      visit = StoreManager.sharedInstance.visit(visitId)
      if let content = visit!.visitcontent {
        visitContent = NSKeyedUnarchiver.unarchiveObjectWithData(content) as? [String : AnyObject]
        userUnionId = (visitContent!["user_union"] as! DbUnion).unid!
        userSectionId = (visitContent!["user_section"] as! DbSection).unid!
      } else {
        visitContent = [String: AnyObject]()
      }
    } else {
      userUnionId = profile!.dbunionid!
      userSectionId = profile!.dbsectionid!
    }
    
    //employer may be pre-selected
    var allSettings = StoreManager.sharedInstance.allSettings()
    if(allSettings.count > 0 && allSettings[0].employer != "") {
        for worksite in allWorksites {
            let unid:String = worksite.unid!.componentsSeparatedByString("-")[0]
            if (unid == allSettings[0].employer as String) {
                lastEmployer = worksite
                break
            }
        }
    }
    
    // MARK: - Employer section
    form
      +++ Section(NSLocalizedString("Arbetsgivare", comment: "Titel på sektionen"))
      <<< AutoCompleteTextRow("employer") {
        $0.title = NSLocalizedString("Arbetsgivare*", comment: "Namnet på arbetsgivaren")
          //Needed to get values in self.form.values()
          if visit != nil {
            $0.value = visit?.employer
          } else if allSettings.count > 0 {
              $0.value = allSettings[0].employer
          }
      }.cellSetup { cell, row in
        (cell.textField as! MPGTextField_Swift).mDelegate = self
        (cell.textField as! MPGTextField_Swift).popoverBackgroundColor = UIColor.lightGrayColor()
        if self.visit != nil {
          cell.textField.text = self.visit?.employer// self.visitContent!["employer"] as? String
        } else if allSettings.count > 0 {
          cell.textField.text = allSettings[0].employer
        }
      }
      
      <<< NameRow("site") {(row : NameRow) -> Void in
        row.title = NSLocalizedString("Arbetsplats", comment: "Var den intervjuade arbetar")
        if visit != nil{
          row.value = visitContent!["site"] as? String
        } else{
            row.value = lastEmployer?.name
        }
        
        row.displayValueFor = {
          if let site = self.site {
            row.value = site
          }
          guard let value = $0 else{
            return nil
          }
          return value
        }
      }
      <<< NameRow("streetaddress") {
        $0.title = NSLocalizedString("Gatuadress", comment: "Arbetsplatsens adress")
        if  visit != nil{
          $0.value = visitContent!["streetaddress"] as? String
        }else{
                $0.value = lastEmployer?.streetaddress
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateStreetAddress {
            self.updateStreetAddress = false
            row.value = self.addressInfo[1]
            row.updateCell()
          }
      }
      <<< NameRow("zipcode") {
        $0.title = NSLocalizedString("Postnummer", comment: "Arbetsplatsens postnummer")
        if visit != nil{
          $0.value = visitContent!["zipcode"] as? String
        }else{
            $0.value = lastEmployer?.zipcode
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateZipCode {
            self.updateZipCode = false
            row.value = self.addressInfo[2]
            row.updateCell()
          }
      }
      <<< TextRow("city") {
        $0.title = NSLocalizedString("Ort", comment: "Arbetsplatsens ort")
        if visit != nil{
          $0.value = visitContent!["city"] as? String
        }
        else{
            $0.value = lastEmployer?.city
        }
        }.cellUpdate {cell, row in
          if self.addressInfo.count > 0 && self.updateCity {
            self.updateCity = false
            row.value = self.addressInfo[3]
            row.updateCell()
          }
      }
      
      // MARK: - Information section
      +++ Section(NSLocalizedString("Uppgifter", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }

      <<< NameRow("interviewee") {
        $0.title = NSLocalizedString("Namn*", comment: "Arbetstagarens namn")
        if  visit != nil{
          $0.value = visit?.name
        }
      }
      <<< EmailRow("email") {
        $0.title = NSLocalizedString("E-post", comment: "Arbetstagarens e-post")
        if visit != nil{
          $0.value = visitContent!["email"] as? String
        }
      }
      <<< PhoneRow("phone") {
        $0.title = NSLocalizedString("Telefon", comment: "Arbetstagarens telefon")
        if visit != nil{
          $0.value = visitContent!["phone"] as? String
        }
      }
      //<<< DateTimeInlineRow("interviewdate") {
        <<< DateInlineRow("interviewdate") {
        $0.title = NSLocalizedString("Datum*", comment: "Arbetstagarens datum")
        if visit != nil{
          $0.value = visit?.interviewdate
        } else {
          $0.value = NSDate()
        }
      }
    
      <<< PickerInlineRow<Project>("user_project") { (row : PickerInlineRow<Project>) -> Void in
        row.title = NSLocalizedString("Tillhör projekt", comment: "Den inloggade informatörens projekt")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as Project).name
        }
        if !allProjects.isEmpty {
          row.options = allProjects
          if allProjects.count == 1 {
            row.value = row.options[0]
          } else {
            let index = row.options.indexOf({$0.unid == "0"}) ?? 0
            row.value = row.options[index]
          }
        }
        if visit != nil{
          row.value = (visitContent!["user_project"] as? Project)
        }
      }
      <<< PickerInlineRow<DbUnion>("user_union") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = NSLocalizedString("Förbund*", comment: "Den inloggade informatörens förbund")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
          //userUnionId is either based on previously saved value, or the profile default value
          let index = row.options.indexOf({$0.unid == userUnionId}) ?? 0
          row.value = row.options[index]
        }
        row.onChange { [weak self] row in
          if row.value! != "" {
            self!.userUnionId = row.value!.unid!
            self!.userSectionId = ""
            updateSection = true
            self?.form.rowByTag("user_section")?.updateCell()
          }
        }
      }
      <<< PickerInlineRow<DbSection>("user_section") { (row : PickerInlineRow<DbSection>) -> Void in
        row.title = NSLocalizedString("Avdelning*", comment: "Den inloggade informatörens avdelning")
        
        row.displayValueFor = {
          guard let value = $0 where !updateSection else{
            return nil
          }
          return (value as DbSection).name
        }
        row.cellSetup {cell, row in
          //userSectionId is either based on previously saved value, or the profile default value
          self.infoForSection(row)
        }
        row.cellUpdate {cell, row in
          if updateSection {
            updateSection = false
            self.infoForSection(row)
            cell.detailTextLabel?.text = row.displayValueFor?(row.value)
          }
        }
      }
      <<< PickerInlineRow<District>("user_district") { (row : PickerInlineRow<District>) -> Void in
        row.title = NSLocalizedString("Distrikt*", comment: "Alla LO:s distrikt")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as District).name
        }
        if !allDistricts.isEmpty {
          row.options = allDistricts
          let index = row.options.indexOf({$0.unid == profile?.districtid}) ?? 0
          row.value = row.options[index]
        }
        if visit != nil{
          row.value = (visitContent!["user_district"] as? District)
        }
      }
      <<< SegmentedRow<String>("user_membership_status"){
        $0.title = NSLocalizedString("Status*", comment: "Medlemsstatus för den intervjuade")
        $0.options = ["Medlem", "Ej medlem", "Okänt"]
        if visit != nil {
            
            
          $0.value = visitContent!["user_membership_status"] as? String
        }
      }
      
      // MARK: - Yes/No queries sections
      +++ Section(NSLocalizedString("Sjukdom", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_illness"){
        $0.title = "Har Du missat att ansöka om ersättning enligt kollektivavtal?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_illness"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1//(cell.titleLabel?.bounds.size.width)!
      }
      +++ Section(NSLocalizedString("Arbetsskada", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_injury"){
        $0.title = "Har Du missat att anmäla skadan och ansöka om ersättning enligt kollektivavtal?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_injury"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {90}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Arbetslöshet", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_unemployment"){
        $0.title = "Har Du missat ersättning enligt kollektivavtal?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_unemployment"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Ålderspension", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_retirement"){
        $0.title = "Har Du missat att göra aktiva val för din avtalspension?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_retirement"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Ålderspension forts.", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_retirement_continued"){
        $0.title = "Har Du missat att ansöka om ersättning från premiebefrielseförsäkringen?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_retirement_continued"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {95}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Dödsfall", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_death"){
        $0.title = "Har Du missat att ansöka om ersättning enligt kollektivavtal?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_death"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Förälder", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_parenthood"){
        $0.title = "Har Du missat att ansöka om ersättning enligt kollektivavtal?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_parenthood"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
      +++ Section(NSLocalizedString("Medlemsförsäkringar", comment: "Titel på sektionen")) { section in
        section.header?.height = {20}
      }
      <<< SegmentedRow<String>("query_membership"){
        $0.title = "Kan Du tänka Dig att bli medlem?"
        $0.options = ["Ja", "Nej"]
        if visit != nil{
          $0.value = visitContent!["query_membership"] as? String
        }else {
          $0.value =  $0.options[1] //OBS! Ta sparade värden om det finns
        }
        }.cellSetup { cell, row in
          cell.height = {75}
          cell.titleLabel?.numberOfLines = 0
          cell.titleLabel?.preferredMaxLayoutWidth = 1
      }
        +++ Section(NSLocalizedString("Övriga kommentarer", comment: "Titel på sektionen")) { section in
            section.header?.height = {20}
        }
      <<< TextAreaRow("comments"){
        $0.placeholder = NSLocalizedString("", comment: "Titel på fältet")
        if visit != nil{
          $0.value = visitContent!["comments"] as? String
        }
        }.cellSetup { cell, row in
          cell.placeholderLabel.textColor = UIColor.blackColor()
      }
      
      +++ Section() { section in
        section.header?.height = {20}
      }
      // MARK: - Save section
      <<< ButtonRow("save_single_visit") {
        $0.title = "Spara"
        }.onCellSelection {  cell, row in
          var values = self.form.values()
          let errors = self.validateForm(values)
          
          if errors.isEmpty {
            var showSignup = false
            if self.visit != nil{
              values["_id"] = self.visit?._id
              values["unid"] = self.visit?.unid
              showSignup = !StoreManager.sharedInstance.signupForVisit((self.visit?._id)!)
              values["signupid"] = self.visit?.signupid
              values["sent"] = self.visit?.sent
            } else {
              values["_id"] = 0
              values["unid"] = ""            
              showSignup = true
              values["signupid"] = 0
            }
            values["visittype"] = VisitType.Single.rawValue
            let id = StoreManager.sharedInstance.addOrUpdateVisitLocally(values)
            
            //update employer (possibly add settings)
            var allSettings = StoreManager.sharedInstance.allSettings()
            let hasSettings = allSettings.count > 0
            var settings = [String:Any?]()
            settings["_id"] = hasSettings ? allSettings[0]._id : 0
            settings["synchronization"] = hasSettings ? allSettings[0].synchronization : true
            settings["no_of_visits"] = hasSettings ? allSettings[0].numberOfVisits : Config.numberOfVisits
            settings["employer"] = (values["employer"] as? String)!
            
            StoreManager.sharedInstance.addOrUpdateSettingsLocally(settings)
            /////////////////////////////////////////
            
            self.processVisit(id, showSignup: showSignup, values: values)
          } else {
            self.showValidationErrors(errors)
          }
      }
      +++ Section()
    
  }
  
  func processVisit(id: Int, showSignup: Bool, values: [String : Any?]) {
    if StoreManager.sharedInstance.synchronizeAutomatically() {
      let item = StoreManager.sharedInstance.visit(id)
      let ids : [Int] = [(item?._id)!]
      self.tryToSendVisit(item!, ids: ids, showSignup: showSignup, values: values)
    } else {
      self.navigate(showSignup, values: values, visitId: id)
    }
  }
  
  func validateForm(values: [String : Any?]) -> [String] {
    var errors: [String] = []
    if values["employer"]! == nil || (values["employer"] as! String).isEmpty {
      errors.append("Arbetsgivare")
    }
    if values["interviewee"]! == nil {
      errors.append("Namn")
    }
    if values["interviewdate"]! == nil {
      errors.append("Datum")
    }
    if values["user_membership_status"]! == nil {
      errors.append("Status")
    }
    if values["phone"]! == nil || (values["phone"] as! String).isEmpty {
      //Do nothing, an empty field is ok
    } else {
      let phone = values["phone"] as! String
      if !phone.isPhoneNumeric() {
        errors.append("Telefonnummerfältet bör enbart innehålla siffror eller tecken för landskod.")
      }
    }
    return errors
  }
  
  func showValidationErrors(errors: [String]) {
    let message = "Följande fält måste fyllas i: \(errors.joinWithSeparator(", "))"
    let alertController = UIAlertController(title: "Valideringsfel", message: message, preferredStyle: .Alert)
    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertController.addAction(defaultAction)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
  
  // MARK: - synchronize data
  func tryToSendVisit(visit: Visit, ids: [Int], showSignup: Bool, values: [String : Any?]) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true

    NetworkManager.sharedInstance.sendData("setMote", parameters: visit.dataToSynchronize()!, completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if let _ = JSON["id"]! {
          StoreManager.sharedInstance.updateVisits(JSON, ids: ids)
          self.navigate(showSignup, values: values, visitId: ids[0])
        } else {
          status.message = NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren")
          self.informUser(status)
        }
      case .Failure(let error):
        if let error = error as NSError? {
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
            self.navigate(showSignup, values: values, visitId: ids[0])
          } else {
            status.message = error.localizedDescription
            self.informUser(status)
          }
          
        }
      }
    })
  }
  
  func navigate(showSignup: Bool, values: [String : Any?], visitId: Int) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if (showSignup) {
      var dict = [String: Any?]()
      dict["name"] = values["interviewee"] as? String
      dict["phone"] = values["phone"] as? String ?? ""
      dict["worksite"] = values["site"] as? String
      dict["mail"] = values["email"] as? String
      dict["city"] = values["city"] as? String ?? ""
      dict["union"] = (values["user_union"] as? DbUnion)?.unid
      let status = (values["user_membership_status"] as? String)?.lowercaseString
      dict["member"] = status == "medlem" ? "Ja" : "Nej"
      dict["visitid"] = visitId
      
      let signup = Signup(attributes: dict)
      
      let navigationVc = storyboard.instantiateViewControllerWithIdentifier("SignupNavigationView") as! UINavigationController
      let signupVc = navigationVc.topViewController as! SignupViewController
      signupVc.signup = signup
      
      if self.revealViewController() != nil {
        self.revealViewController().pushFrontViewController(navigationVc, animated: true)
      } else if self.parentViewController?.revealViewController() != nil {
        self.parentViewController?.revealViewController().pushFrontViewController(navigationVc, animated: true)
      }
    } else {
      let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
      if self.revealViewController() != nil {
        self.revealViewController().pushFrontViewController(vc, animated: true)
      } else if self.parentViewController?.revealViewController() != nil {
        self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
      }
    }
  }
  
  func informUser(status: RequestStatus) {
    let alertController = UIAlertController(title: "Information", message: status.message, preferredStyle: .Alert)
    
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
      UIAlertAction in
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
      if self.revealViewController() != nil {
        self.revealViewController().pushFrontViewController(vc, animated: true)
      } else if self.parentViewController?.revealViewController() != nil {
        self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
      }
    }
    
    // Add the actions
    alertController.addAction(okAction)
    self.presentViewController(alertController, animated: true, completion: nil)
    
  }
  
  // MARK: - Helper functions for inline pickers
  func infoForSection(row: PickerInlineRow<DbSection>) {
    let sections = StoreManager.sharedInstance.sectionsForUnion(userUnionId)
    if !sections.isEmpty {
      row.options = sections
      let index = row.options.indexOf({$0.unid == userSectionId}) ?? 0
      row.value = row.options[index]
    }
  }
  
  // MARK: - Tab title
  
  func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return itemInfo
  }
  
  // MARK: - Autocomplete field implementations
  func dataForPopoverInTextField(textfield: MPGTextField_Swift) -> [Dictionary<String, AnyObject>]
  {
    return sampleData
  }
  
  func textFieldShouldSelect(textField: MPGTextField_Swift) -> Bool{
    return true
  }
  
  func textFieldDidEndEditing(textField: MPGTextField_Swift, withSelection data: Dictionary<String,AnyObject>){
    self.site = data["DisplaySubText"] as? String
    addressInfo = data["CustomObject"] as! [String]
    self.form.rowByTag("site")?.updateCell()
    self.updateStreetAddress = true
    self.updateZipCode = true
    self.updateCity = true
    self.form.rowByTag("streetaddress")?.updateCell()
    self.form.rowByTag("zipcode")?.updateCell()
    self.form.rowByTag("city")?.updateCell()
  }
}
