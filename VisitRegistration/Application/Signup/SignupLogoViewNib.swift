//
//  SignupLogoViewNib.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 02/05/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

class SignupLogoViewNib: UIView {
  
  @IBOutlet weak var imageView: UIImageView!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
}
