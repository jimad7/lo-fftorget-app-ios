	//
//  SupportViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 24/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import Eureka

class SupportViewController: FormViewController {
  var ticket:SupportTicket?
  var ticketId :Int = 0
  var ticketType: String?
  var hideMenu: Bool = false
  
  @IBOutlet var menuButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = NSLocalizedString("Support", comment: "title")

    if hideMenu {
      menuButton.enabled = false
      menuButton.tintColor = UIColor.clearColor()
    } else if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    if ticketId != 0{
      ticket = StoreManager.sharedInstance.supportTicket(ticketId)
    }
    
    let allTicketTypes: [String] = ["Felrapport", "Fråga", "Inloggningsproblem"]
    
    form
      +++ Section(header: NSLocalizedString("Support", comment: "Titel på sektionen"),footer: "Här kan du ställa frågor eller anmäla fel.")
      <<< EmailRow("email") {
        $0.title = NSLocalizedString("E-post*", comment: "E-postadress")
        if ticket != nil{
          $0.value = ticket?.email
        } else if Utils.sharedInstance.isLoggedIn() {
          $0.value = ProfileData.email
        }
      }
      <<< NameRow("subject") {
        $0.title = NSLocalizedString("Ämne*", comment: "Ärendets ämne")
        if  ticket != nil{
          $0.value = ticket?.subject
        }
      }
      <<< TextAreaRow("content"){
        $0.placeholder = NSLocalizedString("Innehåll*", comment: "Ärendets innehåll")
        if ticket != nil{
          $0.value = ticket?.content
        }
      }
      <<< PickerInlineRow<String>("tickettype") { (row : PickerInlineRow<String>) -> Void in
        row.title = NSLocalizedString("Ärendetyp*", comment: "Alla ärendetyper, en statisk lista")
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return value
        }
        if !allTicketTypes.isEmpty {
          row.options = allTicketTypes
        }
        if ticket != nil{
          row.value = ticket?.ticketType
        } else if ticketType != nil {
          row.value = ticketType
        }
      }
      +++ Section()
      
      <<< ButtonRow("send_support_ticket") {
        $0.title = "Skicka in"
        }.onCellSelection {  cell, row in
          var values = self.form.values()
          let errors = self.validateForm(values)
          
          if errors.isEmpty {
            if self.ticket != nil{
              values["_id"] = self.ticket?._id
            }else{
              values["_id"] = 0
            }
            
            let id = StoreManager.sharedInstance.addOrUpdateSupportTicketLocally(values)
            self.processSupportTicket(id)
          } else {
            self.showValidationErrors(errors)
          }
      }
    
      <<< ButtonRow("cancel") {
        $0.title = "Avbryt"
        }
        .onCellSelection {  cell, row in
          if self.hideMenu {
            self.redirectToLogin()
          } else {
            self.navigate()
          }
      }
  }
  
  func processSupportTicket(id: Int) {
    //hideMenu indicates coming from Login view, which means try to send regardless of auto sync setting
    if StoreManager.sharedInstance.synchronizeAutomatically() || hideMenu {
      let item = StoreManager.sharedInstance.supportTicket(id)
      
      self.tryToSendSupportTicket(item!, id: id)
    } else {
        self.navigate()
    }
  }
  
  func tryToSendSupportTicket(ticket: SupportTicket, id: Int) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    
    NetworkManager.sharedInstance.sendQuerystringData("postSupport", parameters: ticket.dataToSynchronize()!, completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if !NetworkManager.sharedInstance.isValid(JSON) {
          status.message = JSON["lastError"] as? String
          self.informUser(status)
        } else {
          //Ok response, inform user and remove the ticket from the db
          status.message = "Supportfråga skickad"
          self.informUser(status)
          StoreManager.sharedInstance.deleteSupportTicket(id)
          if self.hideMenu {
            self.redirectToLogin()
          } else {
            self.navigate()
          }
        }
      case .Failure(let error):
        if let error = error as NSError? {
          
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
          } else {
            status.message = error.localizedDescription
          }
          self.informUser(status)
        }
      }
    })
  }
  
  func navigate() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as! UINavigationController
    self.revealViewController().pushFrontViewController(vc, animated: true)
  }
  
  func redirectToLogin() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let navVC = storyboard.instantiateViewControllerWithIdentifier("LoginView") as! UINavigationController
    self.revealViewController().pushFrontViewController(navVC, animated: true)
  }
  
  func informUser(status: RequestStatus) {
    let alertController = UIAlertController(title: "Information", message: status.message, preferredStyle: .Alert)
    
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
        UIAlertAction in
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
        if self.revealViewController() != nil {
            self.revealViewController().pushFrontViewController(vc, animated: true)
        } else if self.parentViewController?.revealViewController() != nil {
            self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
        }
    }
    
    // Add the actions
    alertController.addAction(okAction)
    self.presentViewController(alertController, animated: true, completion: nil)
    
    
  }
  
  func validateForm(values: [String : Any?]) -> [String] {
    var errors: [String] = []
    if values["email"]! == nil {
      errors.append("E-post")
    }
    if values["subject"]! == nil {
      errors.append("Ämne")
    }
    if values["content"]! == nil {
      errors.append("Innehåll")
    }
    if values["tickettype"]! == nil {
      errors.append("Ärendetyp")
    }
    return errors
  }
  
  func showValidationErrors(errors: [String]) {
    let message = "Följande fält måste fyllas i: \(errors.joinWithSeparator(", "))"
    let alertController = UIAlertController(title: "Valideringsfel", message: message, preferredStyle: .Alert)
    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertController.addAction(defaultAction)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}