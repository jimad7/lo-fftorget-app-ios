//
//  SettingsViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 02/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka

class SettingsViewController : FormViewController{
    
  @IBOutlet var menuButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    var allSettings = StoreManager.sharedInstance.allSettings()
    
    /* Input fields starts here */
    form
      +++ Section("Inställningar")
      
      <<< SwitchRow("synchronization") {
        $0.title = "Synkronisera data automatiskt"
        if !allSettings.isEmpty {
          $0.value = allSettings[0].synchronization
        } else {
          $0.value = true
        }
      }
      <<< SegmentedRow<String>("no_of_visits"){
        $0.title = NSLocalizedString("Antal visade samtal", comment: "Antal samtal som synkroniseras")
        $0.options = ["3", "5", "10"]
        if !allSettings.isEmpty {
          $0.value = String(allSettings[0].numberOfVisits)
        } else {
          $0.value = "5"
        }
      }
      +++ Section()
      /*
        <<< ButtonRow("do_dataDownload") {
            $0.title = "Uppdatera data"
            }
            .onCellSelection {  cell, row in
                self.toast("Hämtar data..")
                NetworkManager.sharedInstance.retrieveAllData({ (status) -> Void in
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    if status.statusCode == RequestStatusCode.Ok {
                        /*
                        Utils.sharedInstance.setValueForKey(defaultsKeys.loggedin, value: true)
                        Utils.sharedInstance.setValueForKey(defaultsKeys.userid, value: ProfileData.id)
                        Utils.sharedInstance.setValueForKey(defaultsKeys.name, value: ProfileData.name)
                        Utils.sharedInstance.setValueForKey(defaultsKeys.fullname, value: ProfileData.fullname)
                        Utils.sharedInstance.setValueForKey(defaultsKeys.email, value: ProfileData.email)
                        */
                        self.showMessage("", message: "Datat är nu uppdaterat")
                    } else {
                        self.showMessage("Felmeddelande - datahämtning", message: status.message!)
                    }
                })
        }
   */
        +++ Section()
        
      <<< ButtonRow("save_settings") {
        $0.title = "Spara inställningar"
        }.onCellSelection {  cell, row in
          var values = self.form.values()
          if !allSettings.isEmpty{
            values["_id"] = allSettings[0]._id
          } else{
            values["_id"] = 0
          }
          _ = StoreManager.sharedInstance.addOrUpdateSettingsLocally(values)
          allSettings = StoreManager.sharedInstance.allSettings()
            self.showMessage("Information", message: "Inställningar sparade")
      }
      
      +++ Section()
  }
 
    
    func showMessage(title: String, message: String) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    func toast(message: String) {
        var style = ToastStyle()
        style.messageColor = UIColor.whiteColor()
        style.backgroundColor = UIColor.lightGrayColor()
        self.view.makeToast(message, duration: 5.0, position: .Center, style: style)
        ToastManager.shared.style = style
        ToastManager.shared.tapToDismissEnabled = true
        ToastManager.shared.queueEnabled = true
    }

    
    
}
