//
//  LoginViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka

class LoginViewController: FormViewController {
    
    
    var lastDatadownload : NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Logga in"
        
        form
            +++ Section(footer: "Ange användarnamn och lösenord för att logga in. För att logga in krävs nätverksuppkoppling och en registrerad användare - för att registrera en användare, kontakta LO.") { section in
                var header = HeaderFooterView<LogoViewNib>(.NibFile(name: "LoginHeader", bundle: nil))
                header.onSetupView = { view, _, _ in
                    
                }
                section.header = header
            }
            <<< NameRow("username") {
                $0.title = NSLocalizedString("Användarnamn", comment: "")
            }
            <<< PasswordRow("pwd") {
                $0.title = NSLocalizedString("Lösenord", comment: "")
            }
            <<< ButtonRow("login_user") {
                $0.title = "Logga in"
                }.onCellSelection {  cell, row in
                    var values = self.form.values()
                    let errors = self.validateForm(values)
                    
                    if errors.isEmpty {
                        self.tryToLogin(values["username"] as! String, pwd: values["pwd"] as! String)
                    } else {
                        self.showValidationErrors(errors)
                    }
            }
            +++ Section()
            <<< ButtonRow("support") {
                $0.title = "Support"
                }.onCellSelection {  cell, row in
                    self.verifySupportConnection()
            }
            +++ Section()
            <<< ButtonRow("forgotpassword") {
                $0.title = "Glömt lösenord"
                }.onCellSelection { cell, row in
                    self.RedirectToForgotPasswordView()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        Utils.sharedInstance.deleteKey(defaultsKeys.loggedin)
        Utils.sharedInstance.deleteKey(defaultsKeys.userid)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tryToLogin(username: String, pwd: String) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let credentialData = "\(username)#\(pwd)".dataUsingEncoding(NSUTF8StringEncoding)!
        let base64Credentials = credentialData.base64EncodedStringWithOptions([])
        var dict : [String: AnyObject] = [String: AnyObject]()
        dict["userinfo"] = base64Credentials
        
        NetworkManager.sharedInstance.sendQuerystringData("getUserAuthorization", parameters: dict, completion: { (result) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            switch result {
            case .Success(let JSON):
                if !NetworkManager.sharedInstance.isValid(JSON) {
                    self.showMessage("Felmeddelande", message : JSON["lastError"] as! String)
                } else if let unid = JSON["id"]! {
                    
                    
                   // self.lastDatadownload = StoreManager.sharedInstance.dataDownload(ProfileData.id)?.lastDatadownload
                    self.lastDatadownload = StoreManager.sharedInstance.dataDownload(unid as! String)?.lastDatadownload
                    
                    StoreManager.sharedInstance.addOrUpdateUsername(unid as! String, username: JSON["user"] as! String)
                    ProfileData.id = unid as! String
   
                    if(self.lastDatadownload == nil) {
                        self.toast("Hämtar data till appen, var god vänta...")
                        self.tryToRetrieveAllData()
                        let values: [String: Any?] = ["unid":ProfileData.id, "lastDatadownload": NSDate()]
                        _ = StoreManager.sharedInstance.updateLatestDownloadDate(values, unid: ProfileData.id)
                    }
                    else {
                        self.tryToRetrieveProfileData()
                    }
                    
                } else {
                    self.showMessage("Ogiltigt svar", message: NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren"))
                }
            case .Failure(let error):
                if let error = error as NSError? {
                    if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                        self.showMessage("Inget nätverk", message: NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren"))
                    } else {
                        self.showMessage("Felmeddelande", message: error.localizedDescription)
                    }
                    self.showMessage("Ogiltigt svar", message: NSLocalizedString("Ogiltigt svar", comment: "Meddelande som ska informera användaren"))
                }
            }
        })
    }
    
    func tryToRetrieveProfileData() {
        NetworkManager.sharedInstance.retrieveProfileData({ (status) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if status.statusCode == RequestStatusCode.Ok {
                
                Utils.sharedInstance.setValueForKey(defaultsKeys.loggedin, value: true)
                Utils.sharedInstance.setValueForKey(defaultsKeys.userid, value: ProfileData.id)
                Utils.sharedInstance.setValueForKey(defaultsKeys.name, value: ProfileData.name)
                Utils.sharedInstance.setValueForKey(defaultsKeys.fullname, value: ProfileData.fullname)
                Utils.sharedInstance.setValueForKey(defaultsKeys.email, value: ProfileData.email)
               
         //      self.showMessage("", message: ProfileData.name)
                
                NSNotificationCenter.defaultCenter().postNotificationName("refresh", object: nil, userInfo: nil)
                self.dismissViewControllerAnimated(true, completion: nil)
                
                self.navigate()
                
            } else {
                self.showMessage("Felmeddelande - datahämtning", message: status.message!)
            }
        })

    }
    
    func tryToRetrieveAllData() {
        NetworkManager.sharedInstance.retrieveAllData({ (status) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if status.statusCode == RequestStatusCode.Ok {
                Utils.sharedInstance.setValueForKey(defaultsKeys.loggedin, value: true)
                Utils.sharedInstance.setValueForKey(defaultsKeys.userid, value: ProfileData.id)
                Utils.sharedInstance.setValueForKey(defaultsKeys.name, value: ProfileData.name)
                Utils.sharedInstance.setValueForKey(defaultsKeys.fullname, value: ProfileData.fullname)
                Utils.sharedInstance.setValueForKey(defaultsKeys.email, value: ProfileData.email)
                self.navigate()
            } else {
                self.showMessage("Felmeddelande - datahämtning", message: status.message!)
            }
        })
    }
    
    
    //Check if a network connection exists by calling getProfile (any existing api method will do) & redirect to ForgotPassword view.
    func RedirectToForgotPasswordView() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        NetworkManager.sharedInstance.receiveData("getProfile", parameters: ["OpenAgent": "1", "id" : "0"], completion: { (result) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            switch result {
            case .Success( _):
                if self.revealViewController() != nil {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navVC = storyboard.instantiateViewControllerWithIdentifier("ForgotPasswordView") as! UINavigationController
                    
                    self.revealViewController().pushFrontViewController(navVC, animated: true)
                    
                    /*
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let row = tableView.cellForRowAtIndexPath(indexPath) as! MainTableViewCell
                     if row.visitType.text == VisitType.Single.rawValue {
                     //performSegueWithIdentifier("ViewSingleItem", sender: row.tag)
                     let navVC = storyboard.instantiateViewControllerWithIdentifier("NewVisitView") as! UINavigationController
                     let singleVisitView = navVC.topViewController as! VisitContainerViewController
                     singleVisitView.visitId = row.tag
                     singleVisitView.visitType = VisitType.Single.rawValue
                     self.revealViewController().pushFrontViewController(navVC, animated: true)
                     */
                    
                    
                }
            case .Failure(let error):
                if let error = error as NSError? {
                    if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                        self.showMessage("Inget nätverk", message: NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren"))
                    } else {
                        self.showMessage("Felmeddelande", message: error.localizedDescription)
                    }
                }
            }
        })
    }
    
    
    
    
    //Check if a network connection exists by calling getProfile (any existing api method will do)
    func verifySupportConnection() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        NetworkManager.sharedInstance.receiveData("getProfile", parameters: ["OpenAgent": "1", "id" : "0"], completion: { (result) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            switch result {
            case .Success( _):
                if self.revealViewController() != nil {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let navVC = storyboard.instantiateViewControllerWithIdentifier("SupportNavigationView") as! UINavigationController
                    let supportVC = navVC.topViewController as! SupportViewController
                    supportVC.hideMenu = true
                    supportVC.ticketType = "Inloggningsproblem"
                    self.revealViewController().pushFrontViewController(navVC, animated: true)
                }
            case .Failure(let error):
                if let error = error as NSError? {
                    if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                        self.showMessage("Inget nätverk", message: NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren"))
                    } else {
                        self.showMessage("Felmeddelande", message: error.localizedDescription)
                    }
                }
            }
        })
    }
    
    func toast(message: String) {
        var style = ToastStyle()
        style.messageColor = UIColor.whiteColor()
        style.backgroundColor = UIColor.lightGrayColor()
        self.view.makeToast(message, duration: 5.0, position: .Center, style: style)
        ToastManager.shared.style = style
        ToastManager.shared.tapToDismissEnabled = true
        ToastManager.shared.queueEnabled = true
    }
    
    func navigate() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
            if self.revealViewController() != nil {
                self.revealViewController().pushFrontViewController(vc, animated: true)
            }
        })
    }
    
    func validateForm(values: [String : Any?]) -> [String] {
        var errors: [String] = []
        if values["username"]! == nil {
            errors.append("Användarnamn")
        }
        if values["pwd"]! == nil {
            errors.append("Lösenord")
        }
        return errors
    }
    
    func showValidationErrors(errors: [String]) {
        let message = "Följande fält måste fyllas i: \(errors.joinWithSeparator(", "))"
        self.showMessage("Valideringsfel", message: message)
    }
    
    func showMessage(title: String, message: String) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
}
