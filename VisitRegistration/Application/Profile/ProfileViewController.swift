//
//  ProfileViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka

class ProfileViewController: FormViewController {
  
  @IBOutlet var menuButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Mitt konto"
    
    if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    let profile = StoreManager.sharedInstance.profile(ProfileData.id)
    let allProjects = StoreManager.sharedInstance.allProjects()
    form
      +++ Section("Personuppgifter")
      <<< LabelRow("name") {
        $0.title = NSLocalizedString("Namn", comment: "")
        $0.value = profile?.name
      }
      <<< LabelRow("email") {
        $0.title = NSLocalizedString("E-post", comment: "")
        $0.value = profile?.email
      }
      <<< LabelRow("phone") {
        $0.title = NSLocalizedString("Telefon", comment: "")
        $0.value = profile?.phone
      }
      <<< LabelRow("union") {
        $0.title = NSLocalizedString("Förbund", comment: "")
        $0.value = profile?.dbunion
      }
      <<< LabelRow("department") {
        $0.title = NSLocalizedString("Avdelning", comment: "")
        $0.value = profile?.dbsection
      }
      <<< LabelRow("district") {
        $0.title = NSLocalizedString("Distrikt", comment: "")
        $0.value = profile?.district
      }
    if let roles = profile?.roles {
      let allRoles = roles.componentsSeparatedByString(",")
      for index in 0...allRoles.count-1 {
          form[0] <<< LabelRow("role\(index)") {
            $0.title = index == 0 ? NSLocalizedString("Uppdrag", comment: "") : ""
            $0.value = allRoles[index]
          }
      }
    } else {
      let labelRow = LabelRow("role") {
        $0.title = NSLocalizedString("Uppdrag", comment: "")
      }
      form[0] <<< labelRow
    }
    if !allProjects.isEmpty {
      for index in 0...allProjects.count-1 {
        if allProjects[index].unid != "0" {
          form[0] <<< LabelRow("project\(index)") {
            $0.title = index == 0 ? NSLocalizedString("Projekt", comment: "") : ""
            $0.value = allProjects[index].name
          }
        }

      }
    } else {
      let labelRow = LabelRow("project") {
          $0.title = NSLocalizedString("Projekt", comment: "")
      }
      form[0] <<< labelRow
    }

  }
  
  override func viewDidAppear(animated: Bool) {
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
