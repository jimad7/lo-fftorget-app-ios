//
//  SignupController.swift
//  VisitRegistration
//
//  Created by Christofer Kihlman on 2016-02-19.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Eureka

class SignupViewController : FormViewController{
  
  @IBOutlet weak var menuButton: UIBarButtonItem!
  
  var signup:Signup? //NB: may be initialized from SingleVisitVC, but then signupId is 0
  var signupId :Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = NSLocalizedString("Rådgivning Folksam", comment: "title")
    
    if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    } else {
      menuButton.enabled = false
      menuButton.image = nil
    }
    let allUnions = StoreManager.sharedInstance.allUnions()
    
    if signupId != 0{
      signup = StoreManager.sharedInstance.signup(signupId)
    }
    
    /* Input fields starts here */
    form
      +++ Section("Uppgifter") { section in
        var header = HeaderFooterView<SignupLogoViewNib>(.NibFile(name: "SignupHeader", bundle: nil))
        header.onSetupView = { view, _, _ in
          
        }
        section.header = header
      }
      
      <<< NameRow("name") {
        $0.title = "Namn:*"
        if signup != nil{
          $0.value = signup?.name
        }
      }
      <<< PhoneRow("phone") {
        $0.title = "Tel/mobil:*"
        if signup != nil{
          $0.value = signup?.phone
        }
      }
      <<< NameRow("mail") {
        $0.title = "Epost:"
        if signup != nil{
          $0.value = signup?.mail
        }
      }
      <<< PickerInlineRow<DbUnion>("dbunion") { (row : PickerInlineRow<DbUnion>) -> Void in
        row.title = "Förbund:"
        row.displayValueFor = {
          guard let value = $0 else{
            return nil
          }
          return (value as DbUnion).name
        }
        if !allUnions.isEmpty {
          row.options = allUnions
          var index = 0
          if signup != nil{
            index = row.options.indexOf({$0.unid == signup?.unionid}) ?? 0
          }
          row.value = row.options[index]
        }
      }
//      <<< CheckRow("member")
//        {
//          $0.title = "Medlem:"
//          if signup != nil{
//            if signup?.member == 0{
//              $0.value = false
//            }else{
//              $0.value = true
//            }
//          }
//      }
      <<< SegmentedRow<String>("member"){
        $0.title = NSLocalizedString("Medlem:", comment: "Status på medlemsskap")
        $0.options = ["Ja", "Nej"]
        if signup != nil{
          if signup?.member == 1{
            $0.value = "Ja"
          } else {
            $0.value = "Nej"
          }
        }
      }
      <<< NameRow("city") {
        $0.title = "Ort:*"
        if signup != nil{
          $0.value = signup?.city
        }
      }
      <<< NameRow("worksite") {
        $0.title = "Arbetsplats:"
        if signup != nil{
          $0.value = signup?.worksite
        }
      }
        <<< TextAreaRow("comments"){
            $0.placeholder = NSLocalizedString("Övriga kommentarer (skriv i detta fält)", comment: "Titel på fältet")
            if signup != nil{
                //$0.value = signup?.comments
            }
            }.cellSetup { cell, row in
                cell.placeholderLabel.textColor = UIColor.blackColor()
        }

      +++ Section()
      
      <<< ButtonRow("send_signup") {
        $0.title = "Skicka till Folksam"
        }.onCellSelection {  cell, row in
          var values = self.form.values()
          let errors = self.validateForm(values)
          
          if errors.isEmpty {
            var visitId: Int = 0
            if self.signup == nil{
              values["_id"] = 0
              values["visitid"] = 0
            }else{
              values["_id"] = self.signup!._id
              values["visitid"] = self.signup!.visitid
              visitId = self.signup!.visitid
            }
            values["union"] = (values["dbunion"] as! DbUnion).unid
            let id = StoreManager.sharedInstance.addOrUpdateSignupLocally(values)
            if visitId > 0 {
              StoreManager.sharedInstance.updateVisitWithSignupId(visitId, signupid: id)
            }
            
            self.processSignup(id)
          } else {
            self.showValidationErrors(errors)
          }
      }
      
      <<< ButtonRow("cancel") {
        $0.title = "Avbryt"
        }
        .onCellSelection {  cell, row in
            self.navigate()
      }
      +++ Section()
  }
  
  func processSignup(id: Int) {
    if StoreManager.sharedInstance.synchronizeAutomatically() {
      let item = StoreManager.sharedInstance.signup(id)
      self.tryToSendSignup(item!, id: id)
    } else {
      let status = RequestStatus()
      status.message = "Rådgivning Folksam sparad"
      self.informUser(status)
    }
  }
  
  func validateForm(values: [String : Any?]) -> [String] {
    var errors: [String] = []
    if values["name"]! == nil || (values["name"] as! String).isEmpty {
      errors.append("Namn")
    }
    if values["phone"]! == nil || (values["phone"] as! String).isEmpty {
      errors.append("Tel/mobil")
    } else {
      let phone = values["phone"] as! String
      if !phone.isPhoneNumeric() {
        errors.append("Telefonnummerfältet bör enbart innehålla siffror eller tecken för landskod.")
      }
    }
    if values["city"]! == nil || (values["city"] as! String).isEmpty {
      errors.append("Ort")
    }
    return errors
  }
  
  func tryToSendSignup(signup: Signup, id: Int) {
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    
    NetworkManager.sharedInstance.sendQuerystringData("postMailFolksam", parameters: signup.dataToSynchronize(), completion: { (result) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      let status = RequestStatus()
      switch result {
      case .Success(let JSON):
        if !NetworkManager.sharedInstance.isValid(JSON) {
          status.message = JSON["lastError"] as? String
          self.informUser(status)
          
        }else{
          StoreManager.sharedInstance.deleteSignup(id, resetVisitConnection: false)
          status.message = "Rådgivning Folksam skickad"
          self.informUser(status)
        }
      case .Failure(let error):
        if let error = error as NSError? {
          if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
            status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
          } else {
            status.message = error.localizedDescription
          }
          self.informUser(status)
        }
      }
    })
  }
  
  func navigate() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as! UINavigationController
    if self.revealViewController() != nil {
      self.revealViewController().pushFrontViewController(vc, animated: true)
    }
  }
  
  func informUser(status: RequestStatus) {
    let alertController = UIAlertController(title: "Information", message: status.message, preferredStyle: .Alert)
    // Create the actions
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
      UIAlertAction in
      self.navigate()
    }
    // Add the actions
    alertController.addAction(okAction)
    self.presentViewController(alertController, animated: true, completion: nil)
    
  }
  func showValidationErrors(errors: [String]) {
    let message = "Följande fält måste rättas: \(errors.joinWithSeparator(", "))"
    let alertController = UIAlertController(title: "Valideringsfel", message: message, preferredStyle: .Alert)
    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertController.addAction(defaultAction)
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}