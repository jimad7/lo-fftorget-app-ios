//
//  MainViewTableCell.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 29/01/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

class MainTableViewCell: UITableViewCell {

  @IBOutlet var visitType: UILabel!
  @IBOutlet var visitNameLabel: UILabel!
  @IBOutlet var dateLabel: UILabel!
  @IBOutlet var employerLabel: UILabel!
  @IBOutlet var visitTypeLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}