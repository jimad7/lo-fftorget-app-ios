//
//  ViewController.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 27/01/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import UIKit
import FontAwesome_swift

class MainViewController: UITableViewController {
  
  @IBOutlet weak var menuButton: UIBarButtonItem!
  var locallySavedInfo : [AnyObject] = []
  var synchronizedVisits : [Visit] = []
  let separatorColor = UITableView().separatorColor
  var isRotating = false
  var shouldStopRotating = false
  var syncLabel: UIView?
  @IBOutlet weak var newVisitButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if !Utils.sharedInstance.isLoggedIn() {
      self.redirectToLogin()
    } else {
      ProfileData.id = Utils.sharedInstance.stringForKey(defaultsKeys.userid)
      ProfileData.name = Utils.sharedInstance.stringForKey(defaultsKeys.name)
      ProfileData.fullname = Utils.sharedInstance.stringForKey(defaultsKeys.fullname)
      ProfileData.email = Utils.sharedInstance.stringForKey(defaultsKeys.email)
    }
    
    if revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = Selector("revealToggle:")
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    self.title = Config.AppName
    self.newVisitButton.layer.borderColor = self.view.tintColor.CGColor
  }
  
  override func viewDidAppear(animated: Bool) {
    if StoreManager.sharedInstance.synchronizeAutomatically() {
      self.tryToSync()
    } else {
      self.initAndReload()
    }
  }
  
  // MARK: - Synchronize section
  func tryToSync() {
    self.toast("Skickar och hämtar data...")
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    NetworkManager.sharedInstance.synchronizeAllData({ (status) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      self.initAndReload()
      if status.statusCode == RequestStatusCode.Error {
        self.showMessage("Meddelande - synkronisering", message: status.message!)
      }
    })
  }
  
  // Called via UITapGestureRecognizer for the label
  func syncLabelTapped(sender: UITapGestureRecognizer) {
    self.syncLabel = sender.view
    self.toast("Skickar och hämtar data...")
    if self.isRotating == false {
      self.syncLabel!.rotate360Degrees(completionDelegate: self)
      self.isRotating = true
    }
    
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    NetworkManager.sharedInstance.synchronizeAllData({ (status) -> Void in
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      self.shouldStopRotating = true
      self.initAndReload()
      if status.statusCode == RequestStatusCode.Error {
        self.showMessage("Meddelande - synkronisering", message: status.message!)
      }
    })
  }
  
  override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
    if self.shouldStopRotating == false {
      self.syncLabel!.rotate360Degrees(completionDelegate: self)
    } else {
      self.reset()
    }
  }
  
  func reset() {
    self.isRotating = false
    self.shouldStopRotating = false
  }
  
  func toast(message: String) {
    var style = ToastStyle()
    style.messageColor = UIColor.whiteColor()
    style.backgroundColor = UIColor.lightGrayColor()
    self.view.makeToast(message, duration: 3.0, position: .Bottom, style: style)
    ToastManager.shared.style = style
    ToastManager.shared.tapToDismissEnabled = true
    ToastManager.shared.queueEnabled = true
  }
  
  func showMessage(title: String, message: String) {
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
      let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
      alertController.addAction(defaultAction)
      self.presentViewController(alertController, animated: true, completion: nil)
    })
  }
  
  // MARK: -
  @IBAction func newVisit(sender: AnyObject) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let navigationVc = storyboard.instantiateViewControllerWithIdentifier("NewVisitView") as! UINavigationController
    self.revealViewController().pushFrontViewController(navigationVc, animated: true)
  }
  
  func redirectToLogin() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let navVC = storyboard.instantiateViewControllerWithIdentifier("LoginView") as! UINavigationController
    self.revealViewController().pushFrontViewController(navVC, animated: true)
  }
  
  func initAndReload() {
    self.initData()
    self.tableView.reloadData()
  }
  
  func initData() {
    var settings = StoreManager.sharedInstance.allSettings()
    let allInfo = StoreManager.sharedInstance.allInfo()
    let allVisits = StoreManager.sharedInstance.allVisits()
    self.locallySavedInfo = allInfo
    let syncedVisits = allVisits.filter({$0.status == VisitStatus.Synced.rawValue}).sort{$0.sortkey > $1.sortkey}
    var visits = [Visit]()
    var index = 0
    let numberOfVisits = settings.count > 0 ? settings[0].numberOfVisits : Config.numberOfVisits
    for visit in syncedVisits{
        index += 1
        if(numberOfVisits >= index)
        {
            visits.append(visit)
        }
    }
    self.synchronizedVisits = visits
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // Return the number of sections.
    return 2
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // Return the number of rows in the section.
    return section == 0 ? locallySavedInfo.count : synchronizedVisits.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MainTableViewCell
    
    // Configure the cell...
    switch (indexPath.section) {
    case 0:
      let localInfo = InfoContainer(object: locallySavedInfo[indexPath.row])
      cell.visitTypeLabel.text = localInfo!.infotype.iconForInfoType()
      if(localInfo!.name != ""){
      cell.visitNameLabel.text = localInfo!.name
      }else{
        cell.visitNameLabel.text = "Gruppsamtal"
      }
      
      cell.tag = localInfo!._id
      cell.employerLabel.text = localInfo!.employer
      cell.visitType.text = localInfo!.infotype
      cell.dateLabel.text = localInfo!.dateString
    case 1:
      let syncedVisit = synchronizedVisits[indexPath.row]
      cell.visitTypeLabel.text = syncedVisit.visittype.iconForType()
    
      if(syncedVisit.name != ""){
        cell.visitNameLabel.text = syncedVisit.name
      }else{
        cell.visitNameLabel.text = "Gruppsamtal"
      }
      
      
      cell.tag = syncedVisit._id
      cell.visitType.text = syncedVisit.visittype
      cell.employerLabel.text = syncedVisit.employer
      if let date = syncedVisit.interviewdate {
        cell.dateLabel.text = date.stringFromDateWithFormat(Config.shortDateFormat)
      } else {
        cell.dateLabel.text = ""
      }
    default:
      cell.textLabel?.text = "Other"
    }
    
    return cell
  }
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let  headerCell = tableView.dequeueReusableCellWithIdentifier("HeaderCell") as! MainTableHeaderCell
    switch (section) {
    case 0:
      headerCell.headerLabel.text = "Ej synkroniserade ärenden"
      headerCell.headerLabel.textColor = UIColor.orangeColor()
      headerCell.syncLabel.userInteractionEnabled = true
      let aSelector : Selector = #selector(MainViewController.syncLabelTapped(_:))
      let tapGesture = UITapGestureRecognizer(target: self, action: aSelector)
      tapGesture.numberOfTapsRequired = 1
      headerCell.syncLabel.addGestureRecognizer(tapGesture)
    case 1:
      headerCell.headerLabel.text = "Senast synkroniserade samtal"
      headerCell.syncLabel.text = ""
    default:
      headerCell.headerLabel.text = "Övrigt" //Should never hit this line
    }
    let border = UIView(frame: CGRectMake(0,39,self.view.bounds.width,1))
    border.backgroundColor = separatorColor
    let topBorder = UIView(frame: CGRectMake(0,0,self.view.bounds.width,1))
   
    topBorder.backgroundColor = separatorColor
    //headerCell.addSubview(border)
    headerCell.contentView.backgroundColor = UIColor(hexString: "#f6f6f5ff")//UIColor.lightGrayColor().colorWithAlphaComponent(0.1)

    headerCell.contentView.addSubview(border)
    headerCell.contentView.addSubview(topBorder)
    return headerCell.contentView
  }
  
  override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return indexPath.section == 0 ? true : false
  }
  
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if (editingStyle == UITableViewCellEditingStyle.Delete) {
      switch indexPath.section {
        case 0:
          let localInfo = InfoContainer(object: locallySavedInfo[indexPath.row])!
          StoreManager.sharedInstance.deleteInfoContainer(localInfo.infotype, id: localInfo._id)
          self.initAndReload()
        case 1: //For now, this section should not have delete functionality
          self.initAndReload()
        default: break
      }
    }
  }
  
  override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
    footerView.backgroundColor = UIColor.whiteColor()
    let topBorder = UIView(frame: CGRectMake(0,0,self.view.bounds.width,1))
    
    topBorder.backgroundColor = separatorColor
    footerView.addSubview(topBorder)
    return footerView
  }
  
  override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40.0
  }
  
  override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 20.0
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let row = tableView.cellForRowAtIndexPath(indexPath) as! MainTableViewCell
    if row.visitType.text == VisitType.Single.rawValue {
      //performSegueWithIdentifier("ViewSingleItem", sender: row.tag)
      let navVC = storyboard.instantiateViewControllerWithIdentifier("NewVisitView") as! UINavigationController
      let singleVisitView = navVC.topViewController as! VisitContainerViewController
      singleVisitView.visitId = row.tag
      singleVisitView.visitType = VisitType.Single.rawValue
      self.revealViewController().pushFrontViewController(navVC, animated: true)
    }
    else if row.visitType.text == VisitType.Group.rawValue {
      let navVC = storyboard.instantiateViewControllerWithIdentifier("NewVisitView") as! UINavigationController
      let groupVisitView = navVC.topViewController as! VisitContainerViewController
      groupVisitView.visitType = VisitType.Group.rawValue
      groupVisitView.visitId = row.tag
      self.revealViewController().pushFrontViewController(navVC, animated: true)
    }
    else if row.visitType.text == InfoType.Support.rawValue {
      let navigationVc = storyboard.instantiateViewControllerWithIdentifier("SupportNavigationView") as! UINavigationController
      let supportVc = navigationVc.topViewController as! SupportViewController
      supportVc.ticketId = row.tag
      self.revealViewController().pushFrontViewController(navigationVc, animated: true)
    }
    else if row.visitType.text == InfoType.Signup.rawValue {
      let navigationVc = storyboard.instantiateViewControllerWithIdentifier("SignupNavigationView") as! UINavigationController
      let signupVc = navigationVc.topViewController as! SignupViewController
      signupVc.signupId = row.tag
      self.revealViewController().pushFrontViewController(navigationVc, animated: true)
    }
    
  }
  
  func informUser(status: RequestStatus) {
    let alertController = UIAlertController(title: "Information", message:
      status.message, preferredStyle: UIAlertControllerStyle.Alert)
    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
    
    self.presentViewController(alertController, animated: true, completion: nil)
  }
}

