//
//  MainAccountController.swift
//  VisitRegistration
//
//  Created by Christofer Kihlman on 2016-02-11.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import UIKit
import FontAwesome_swift

class MainAccountController : UITableViewController{
 
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        

        
    }

}

