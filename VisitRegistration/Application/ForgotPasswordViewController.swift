//
//  ForgotPasswordViewController.swift
//  VisitRegistration
//
//  Created by Jimmie Espling on 2016-11-04.
//  Copyright © 2016 DL2 AB. All rights reserved.
//

import Foundation
import Eureka

class ForgotPasswordViewController: FormViewController {
 
    var forgotPassword:ForgotPassword?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Glömt lösenord", comment: "title")

        self.forgotPassword = ForgotPassword()
    
    form
    +++ Section(header: NSLocalizedString("Glömt lösenord", comment: "Titel på sektionen"),footer: "Här kan du återställa ditt lösenord")
    <<< EmailRow("email") {
    $0.title = NSLocalizedString("E-postadress*", comment: "E-postadress")
    if forgotPassword != nil{
    $0.value = forgotPassword?.email
    }     }
    <<< NameRow("pnr") {
        
        $0.placeholder = NSLocalizedString("YYMMDDnnnn", comment: "YYMMDDnnnn")
        $0.title = NSLocalizedString("Personnummer*", comment: "Personnummer")
        if  forgotPassword != nil{
            $0.value = forgotPassword?.pnr
        }
        }
        
      
        
        +++ Section()
        
        <<< ButtonRow("send_forgot_password") {
            $0.title = "Återställ lösenord"
            }.onCellSelection {  cell, row in
                let values = self.form.values()
                let errors = self.validateForm(values)
         
                let parameters: [String: AnyObject] = self.getParameters(values)
                
                if errors.isEmpty {
                    self.resetPassword(parameters)
                } else {
                    self.showValidationErrors(errors)
                }
        }
        
        <<< ButtonRow("cancel") {
            $0.title = "Avbryt"
            }
            .onCellSelection {  cell, row in
               
                    self.redirectToLogin()
               
        }
 }
    
    
    func resetPassword(values: [String: AnyObject]) {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        NetworkManager.sharedInstance.sendQuerystringRegistrationData("aSendPassword", parameters: values, completion: { (result) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            let status = RequestStatus()
            switch result {
            case .Success(let JSON):
                if !NetworkManager.sharedInstance.isRegValid(JSON) {
                    status.message = JSON["msg"] as? String
                    self.informUser(status)
                } else {
                    status.message = "Begäran om nytt lösenord har skickats."
                    self.informUser(status)
                    self.redirectToLogin()
                }
            case .Failure(let error):
                if let error = error as NSError? {
                    
                    if error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                        status.message = NSLocalizedString("Nätverksproblem", comment: "Meddelande som ska informera användaren")
                    } else {
                        status.message = error.localizedDescription
                    }
                    self.informUser(status)
                }
            }
        })
    }

    
    
    
    func redirectToLogin() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navVC = storyboard.instantiateViewControllerWithIdentifier("LoginView") as! UINavigationController
        self.revealViewController().pushFrontViewController(navVC, animated: true)
    }

    func getParameters(values: [String : Any?]) -> [String: AnyObject] {
            let newDict = values.reduce([String: AnyObject](), combine: { accumulator, pair in
            var temp = accumulator
            temp[pair.0] = pair.1 as? AnyObject
            return temp
        })
        
        return newDict
    }

    
    func validateForm(values: [String : Any?]) -> [String] {
        var errors: [String] = []
        if values["email"]! == nil {
            errors.append("E-post")
        }
        if values["pnr"]! == nil {
            errors.append("pnr")
        }
        return errors
    }
    
    func showValidationErrors(errors: [String]) {
        let message = "Följande fält måste fyllas i: \(errors.joinWithSeparator(", "))"
        let alertController = UIAlertController(title: "Valideringsfel", message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    
    func informUser(status: RequestStatus) {
        let alertController = UIAlertController(title: "Information", message: status.message, preferredStyle: .Alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
            if self.revealViewController() != nil {
                self.revealViewController().pushFrontViewController(vc, animated: true)
            } else if self.parentViewController?.revealViewController() != nil {
                self.parentViewController?.revealViewController().pushFrontViewController(vc, animated: true)
            }
        }
        
        // Add the actions
        alertController.addAction(okAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }

    
    
    
}
