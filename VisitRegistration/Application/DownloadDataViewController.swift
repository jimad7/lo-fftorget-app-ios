//
//  DownloadDataViewController.swift
//  VisitRegistration
//
//  Created by Jimmie Espling on 2016-11-29.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import Eureka

class DownloadDataViewController : FormViewController{
    
    
    var downloadData:DownloadData?
    var downloadDataId :Int = 0
    var lastDatadownload : NSDate?
    var lastDatadownloadAsString : String = ""
    let myRow = LabelRow("datum")
    var didDownloadData = false;
    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.lastDatadownload = StoreManager.sharedInstance.dataDownload(ProfileData.id)?.lastDatadownload
            if(self.lastDatadownload == nil) {
                
            }
            else {
                //   dateFormatter.dateFormat = "EEE, dd MMM yyy hh:mm:ss +zzzz"
                self.lastDatadownloadAsString = (self.getFormattedDate(self.lastDatadownload!))

            }
        }
        
        form
            +++ Section()
            
            <<< LabelRow("datum") {
                $0.title = NSLocalizedString("Senast nedladdning av data", comment: "")
                $0.value = self.lastDatadownloadAsString
                }.cellUpdate({ (cell, row) in
                    if(self.didDownloadData == true) {
                        self.didDownloadData = false
                        //                        cell.detailTextLabel?.text = self.getFormattedDate(NSDate(), dagar: 5)
                        row.value = self.lastDatadownloadAsString
                        row.updateCell()
                    }
                    
                })
            
            <<< ButtonRow("do_dataDownload") {
                $0.title = "Uppdatera data"
                }
                .onCellSelection {  cell, row in
                    self.toast("Hämtar data..")
                    NetworkManager.sharedInstance.retrieveAllData({ (status) -> Void in
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                        if status.statusCode == RequestStatusCode.Ok {
                            let values: [String: Any?] = ["unid":ProfileData.id, "lastDatadownload": NSDate()]
                            _ = StoreManager.sharedInstance.updateLatestDownloadDate(values, unid: ProfileData.id)
                            self.lastDatadownloadAsString = self.getFormattedDate(NSDate())
                            self.didDownloadData = true;
                            self.form.rowByTag("datum")?.updateCell()
                            self.showMessage("", message: "Datat är nu uppdaterat")
                            self.navigate()
                        } else {
                            self.showMessage("Felmeddelande - datahämtning", message: status.message!)
                        }
                    })
        }
        
    }
    
    
    func getFormattedDate(datum: NSDate)-> String{
        let dateformatter = NSDateFormatter()
        
        dateformatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateformatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        // dateFormatter.timeZone = NSTimeZone(name: "CEDT")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return (dateFormatter.stringFromDate(datum))
        //return (dateFormatter.stringFromDate(NSDate()))
        
    }
    
    
    func showMessage(title: String, message: String) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    func toast(message: String) {
        var style = ToastStyle()
        style.messageColor = UIColor.whiteColor()
        style.backgroundColor = UIColor.lightGrayColor()
        self.view.makeToast(message, duration: 5.0, position: .Center, style: style)
        ToastManager.shared.style = style
        ToastManager.shared.tapToDismissEnabled = true
        ToastManager.shared.queueEnabled = true
    }
    func navigate() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("FirstView") as? UINavigationController
            if self.revealViewController() != nil {
                self.revealViewController().pushFrontViewController(vc, animated: true)
            }
        })
    }

    
    
}