//
//  Employer.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 06/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class Employer: NSObject, Storable, Comparable {
  var _id: Int = 0 // Notice that 'Int?' is not supported. Use NSNumber? instead
  var name: String?
  var unid: String?
  var modified : NSDate? = NSDate()
  
  override required init() {
    super.init()
  }
  
  init?(attributes: [String : AnyObject]) {
    super.init()
    guard let name = attributes["name"] as? String,
    let unid = attributes["id"] as? String
      else {
        return nil
    }
    
    self.name = name
    self.unid = unid
  }
}

func < (lhs: Employer, rhs: Employer) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: Employer, rhs: Employer) -> Bool {
  return lhs._id == rhs._id
}

extension Employer: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}
