//
//  Visit.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 08/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

import SwiftyDB

public enum VisitType: String {
  case Single = "enskildinformation"
  case Group = "gruppinformation"
}
enum VisitStatus: Int {
  case LocallySaved = 0
  case Synced = 1
}
class Visit: NSObject, Storable, Comparable {
  var _id: Int = 0
  var name: String?
  var unid: String?
  var status: Int = VisitStatus.LocallySaved.rawValue
  var modified : NSDate? = NSDate()
  var interviewee: String?
  var interviewdate : NSDate?
  var employer: String?
  var visittype: String = VisitType.Single.rawValue
  var userid: String? //32-char
  var username: String?
  var projectid: String?
  var signupid: Int = 0
  var sent: Int = 0 //indicates that the visit data comes from back-end, meaning NO signup should be displayed
  var sortkey: String? = ""
  var visitcontent: NSData? //For locally saved visits, the main part is saved as a blob

  override required init() {
    super.init()
  }
  
  //Contructor called from addOrUpdateVisitLocally, single and previously saved group visits (since they are one-by-one)
  init?(attributes: [String : Any?]) {
    super.init()
    guard
      let interviewee = attributes["interviewee"] as? String,
      let interviewdate = attributes["interviewdate"] as? NSDate,
      let employer = attributes["employer"] as? String,
      let visittype = attributes["visittype"] as? String,
      let unid = attributes["unid"] as? String
      else {
        return nil
    }
    var dict = [String: AnyObject]()
    for value in attributes {
      dict[value.0] = value.1 as? AnyObject
    }
    self.visitcontent = NSKeyedArchiver.archivedDataWithRootObject(dict)
    self.interviewee = interviewee
    self.interviewdate = interviewdate
    self.employer = employer
    self.visittype = visittype
    self.userid = ProfileData.id
    self.username = ProfileData.fullname
    self.name = interviewee
    self.unid = unid
    self._id = (attributes["_id"] as? Int)!
    if (self._id > 0) {
      self.sent = (attributes["sent"] as? Int)!
    }
    if let project = attributes["user_project"] as? Project {
      self.projectid = project.unid
    } else {
      self.projectid = "0"
    }

    if let signupid = attributes["signupid"] {
      self.signupid = signupid as! Int
    }
  }

  //Constructor called from addGroupVisitsLocally (when saving new group visits)
  init?(attributes: [String : Any?], groupNo: Int) {
    super.init()
    guard
      let interviewdate = attributes["interviewdate"] as? NSDate,
      let employer = attributes["employer"] as? String,
      let visittype = attributes["visittype"] as? String,
      let project = attributes["user_project"] as? Project
      else {
        return nil
    }
    var dict = [String: AnyObject]()
    
    /* NB: Compared to Android and Windows, there are some keys in the dict (tfa, agb, Namn etc) that are not set here
    - instead, they are initialized when retrieving the data in dataForGroupVisit before sending */

    dict["site"] = attributes["site"] as? String
    dict["streetaddress"] = attributes["streetaddress"] as? String
    dict["zipcode"] = attributes["zipcode"] as? String
    dict["city"] = attributes["city"] as? String
    dict["user_union1"] = attributes["user_union\(groupNo)"] as? DbUnion
    dict["user_section1"] = attributes["user_section\(groupNo)"] as? DbSection
    dict["user_district1"] = attributes["user_district\(groupNo)"] as? District
    dict["group_membership_status_yes1"] = attributes["group_membership_status_yes\(groupNo)"] as? Int
    dict["group_membership_status_no1"] = attributes["group_membership_status_no\(groupNo)"] as? Int
    dict["group_membership_status_unknown1"] = attributes["group_membership_status_unknown\(groupNo)"] as? Int
    dict["query_illness1"] = attributes["query_illness\(groupNo)"] as? Int
    dict["query_injury1"] = attributes["query_injury\(groupNo)"] as? Int
    dict["query_unemployment1"] = attributes["query_unemployment\(groupNo)"] as? Int
    dict["query_retirement1"] = attributes["query_retirement\(groupNo)"] as? Int
    dict["query_retirement_continued1"] = attributes["query_retirement_continued\(groupNo)"] as? Int
    dict["query_death1"] = attributes["query_death\(groupNo)"] as? Int
    dict["query_parenthood1"] = attributes["query_parenthood\(groupNo)"] as? Int
    dict["query_membership1"] = attributes["query_membership\(groupNo)"] as? Int
    dict["comments"] = attributes["comments"] as? String
    dict["user_project"] = project
    self.visitcontent = NSKeyedArchiver.archivedDataWithRootObject(dict)
    self.interviewee = ""
    self.interviewdate = interviewdate
    self.employer = employer
    self.visittype = visittype
    self.userid = ProfileData.id
    self.username = ProfileData.fullname
    self.name = interviewee
    //self.unid = unid
    self._id = (attributes["_id"] as? Int)!
    self.projectid = project.unid
  }
  
  //Constructor used from synchronization - addOrUpdateVisits
  init?(attributes: [String : AnyObject]) {
    super.init()
    guard let name = attributes["interviewee"] as? String,
      let unid = attributes["id"] as? String,
      let employer = attributes["employer"] as? String,
      let type = attributes["type"] as? String,
      let dt = attributes["interviewdate"] as? String,
      let sortkey = attributes["sortkey"] as? String
      else {
        return nil
    }
    
    var dict = [String: AnyObject]()
    dict["site"] = attributes["arbetsplats"]
    dict["city"] = attributes["ort"]
    dict["zipcode"] = attributes["postnummer"]
    dict["streetaddress"] = attributes["gatuadress"]
    dict["phone"] = attributes["phone"]
    dict["email"] = attributes["epost"]
    dict["comments"] = attributes["kommentar"]
    let member = attributes["medlem"] as! String
    dict["user_membership_status"] = member == "ja" ? "Medlem" : (member == "nej" ? "Ej medlem" : "Okänt")
    let unionid = attributes["forbund"] as! String
    let dbunion = StoreManager.sharedInstance.dbunion(unionid)
    let sectionid = attributes["avdelning"] as! String
    let dbsection = StoreManager.sharedInstance.dbsection(sectionid)
    let districtid = attributes["distrikt"] as! String
    let district = StoreManager.sharedInstance.district(districtid)
    let projectid = attributes["projekt"] as! String
    let project = StoreManager.sharedInstance.project(projectid)
    let suffix = type == VisitType.Single.rawValue ? "" : "1"
    dict["user_union\(suffix)"] = dbunion
    dict["user_section\(suffix)"] = dbsection
    dict["user_district\(suffix)"] = district
    dict["user_project"] = project
    
    if type == VisitType.Single.rawValue {
      dict["query_illness"] = (attributes["ags"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_injury"] = (attributes["tfa"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_unemployment"] = (attributes["agb"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_retirement"] = (attributes["avtalspension"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_retirement_continued"] = (attributes["premiebefrielse"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_death"] = (attributes["tglagl"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_parenthood"] = (attributes["fpt"] as! Int) == 1 ? "Ja" : "Nej"
      dict["query_membership"] = (attributes["intresseradavmedlemsskap"] as! Int) == 1 ? "Ja" : "Nej"
    } else if type == VisitType.Group.rawValue {
      dict["query_illness1"] = attributes["ags"]
      dict["query_injury1"] = attributes["tfa"]
      dict["query_unemployment1"] = attributes["agb"]
      dict["query_retirement1"] = attributes["avtalspension"]
      dict["query_retirement_continued1"] = attributes["premiebefrielse"]
      dict["query_death1"] = attributes["tglagl"]
      dict["query_parenthood1"] = attributes["fpt"]
      dict["query_membership1"] = attributes["intresseradavmedlemsskap"]
      dict["group_membership_status_yes1"] = attributes["antalmedlemmar"]
      dict["group_membership_status_no1"] = attributes["antalejmedlemmar"]
      dict["group_membership_status_unknown1"] = attributes["antalokanda"]
    }
    self.visitcontent = NSKeyedArchiver.archivedDataWithRootObject(dict)
    
    self.name = name
    self.interviewee = name
    self.unid = unid
    self.employer = employer
    self.visittype = type
    if let tmp = dt.dateFromStringWithFormat(Config.dateFormat) {
      self.interviewdate = tmp
    }
    self.sent = 1
    self.sortkey = sortkey
  }
}

func < (lhs: Visit, rhs: Visit) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: Visit, rhs: Visit) -> Bool {
  return lhs._id == rhs._id
}

extension Visit: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}

extension Visit {
  func dataToSynchronize() -> [String: AnyObject]? {
    if self.visittype == VisitType.Single.rawValue {
      return dataForSingleVisit()
    } else {
      return dataForGroupVisit()
    }
  }
  
  func dataForSingleVisit() -> [String: AnyObject]? {
    let dbDictionary:[String:AnyObject] = NSKeyedUnarchiver.unarchiveObjectWithData(self.visitcontent!) as! [String : AnyObject]
    var dict : [String: AnyObject] = [String: AnyObject]()
    dict["id"] = ProfileData.id
    dict["informator"] = ProfileData.fullname
    let union = dbDictionary["user_union"] as! DbUnion
    dict["informatorforbund"] = union.name
    dict["informatorforbundid"] = union.unid
    let district = dbDictionary["user_district"] as! District
    dict["informatordistrikt"] = district.name
    dict["informatordistriktid"] = district.unid
    let dbsection = dbDictionary["user_section"] as! DbSection
    dict["informatoravd"] = dbsection.name
    dict["informatoravdid"] = dbsection.unid
    dict["motesid"] = self.unid?.characters.count > 1 ? self.unid : ""
    dict["Type"] = self.visittype
    dict["Arbetsgivare"] = self.employer
    dict["Arbetsplats"] = dbDictionary["site"] ?? ""
    dict["Gatuadress"] = dbDictionary["streetaddress"] ?? ""
    dict["Postnummer"] = dbDictionary["zipcode"] ?? ""
    dict["Ort"] = dbDictionary["city"] ?? ""
    dict["Namn"] = self.interviewee
    dict["epost"] = dbDictionary["email"] ?? ""
    dict["phone"] = dbDictionary["phone"] ?? ""
    dict["Datum"] = self.interviewdate!.stringFromDateWithFormat(Config.dateFormat)
    dict["projekt"] = self.projectid
    dict["Forbund0"] = union.unid
    dict["Avdelning0"] = dbsection.unid
    dict["Distrikt0"] = district.unid
    //TODO: handle unknown medlemsstatus?
    if((dbDictionary["user_membership_status"] as! String).lowercaseString == "medlem"){
        dict["medlem"] = "ja"
    }else if((dbDictionary["user_membership_status"] as! String).lowercaseString == "ej medlem"){
        dict["medlem"] = "nej"
    }else{
        dict["medlem"] = "x"
    }
  //  dict["medlem"] = (dbDictionary["user_membership_status"] as! String).lowercaseString == "medlem" ? "ja" : "nej"
    dict["antalMedlemmar0"] = "0"
    dict["antalEjMedlemmar0"] = "0"
    dict["antalOkanda0"] = "0"
    dict["ags0"] = "0"
    dict["ags"] = (dbDictionary["query_illness"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["tfa0"] = "0"
    dict["tfa"] = (dbDictionary["query_injury"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["agb0"] = "0"
    dict["agb"] = (dbDictionary["query_unemployment"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["avtalspension0"] = "0"
    dict["avtalspension"] = (dbDictionary["query_retirement"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["premiebefrielse0"] = "0"
    dict["premiebefrielse"] = (dbDictionary["query_retirement_continued"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["tglagl0"] = "0"
    dict["tglagl"] = (dbDictionary["query_death"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["fpt0"] = "0"
    dict["fpt"] = (dbDictionary["query_parenthood"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["intresseradAvMedlemsskap0"] = "0"
    dict["intresseradAvMedlemsskap"] = (dbDictionary["query_membership"] as! String).lowercaseString == "ja" ? "1" : "0"
    dict["Kommentar"] = dbDictionary["comments"] ?? ""
    
    return dict
  }

  func dataForGroupVisit() -> [String: AnyObject]? {
    let dbDictionary:[String:AnyObject] = NSKeyedUnarchiver.unarchiveObjectWithData(self.visitcontent!) as! [String : AnyObject]
    var dict : [String: AnyObject] = [String: AnyObject]()
    dict["id"] = ProfileData.id
    dict["informator"] = ProfileData.fullname
    
    let union = dbDictionary["user_union1"] as! DbUnion
    dict["informatorforbund"] = union.name
    dict["informatorforbundid"] = union.unid
    let district = dbDictionary["user_district1"] as! District
    dict["informatordistrikt"] = district.name
    dict["informatordistriktid"] = district.unid
    let dbsection = dbDictionary["user_section1"] as! DbSection
    dict["informatoravd"] = dbsection.name
    dict["informatoravdid"] = dbsection.unid
    dict["motesid"] = self.unid?.characters.count > 1 ? self.unid : ""
    dict["Type"] = self.visittype
    dict["Arbetsgivare"] = self.employer
    dict["Arbetsplats"] = dbDictionary["site"] ?? ""
    dict["Gatuadress"] = dbDictionary["streetaddress"] ?? ""
    dict["Postnummer"] = dbDictionary["zipcode"] ?? ""
    dict["Ort"] = dbDictionary["city"] ?? ""
    dict["Namn"] = self.interviewee
    dict["Datum"] = self.interviewdate!.stringFromDateWithFormat(Config.dateFormat)
    dict["projekt"] = self.projectid
    dict["Forbund0"] = union.unid
    dict["Avdelning0"] = dbsection.unid
    dict["Distrikt0"] = district.unid
    dict["medlem"] = ""
    
    //These dbDictionary values are checked and set to 0 if nil in GroupVisitController (when saving)
    dict["antalMedlemmar0"] = dbDictionary["group_membership_status_yes1"]// as! Int
    dict["antalEjMedlemmar0"] = dbDictionary["group_membership_status_no1"]// as! Int
    dict["antalOkanda0"] = dbDictionary["group_membership_status_unknown1"]// as! Int
    dict["ags"] = "0"
    dict["ags0"] = dbDictionary["query_illness1"]// as! Int
    dict["tfa"] = "0"
    dict["tfa0"] = dbDictionary["query_injury1"]// as! Int
    dict["agb"] = "0"
    dict["agb0"] = dbDictionary["query_unemployment1"]// as! Int
    dict["avtalspension"] = "0"
    dict["avtalspension0"] = dbDictionary["query_retirement1"]// as! Int
    dict["premiebefrielse"] = "0"
    dict["premiebefrielse0"] = dbDictionary["query_retirement_continued1"]// as! Int
    dict["tglagl"] = "0"
    dict["tglagl0"] = dbDictionary["query_death1"] //as! Int
    dict["fpt"] = "0"
    dict["fpt0"] = dbDictionary["query_parenthood1"] //as! Int
    dict["intresseradAvMedlemsskap"] = "0"
    dict["intresseradAvMedlemsskap0"] = dbDictionary["query_membership1"] //as! Int
    
    dict["Kommentar"] = dbDictionary["comments"] ?? ""
    
    return dict
  }
}









