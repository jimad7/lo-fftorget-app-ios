//
//  SupportTicket.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 24/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class SupportTicket: NSObject, Storable, Comparable {
  var _id: Int = 0
  var email: String?
  var subject: String?
  var content: String?
  var ticketType: String?
  var status: Int = InfoStatus.LocallySaved.rawValue
  var modified : NSDate? = NSDate()
  
  override required init() {
    super.init()
  }
  
  init?(attributes: [String : Any?]) {
    super.init()
    guard
      let email = attributes["email"] as? String,
      let subject = attributes["subject"] as? String,
      let ticketType = attributes["tickettype"] as? String,
      let content = attributes["content"] as? String
      else {
        return nil
    }
    
    if let id = attributes["_id"] as? Int {
      self._id = id
    }    
    self.email = email
    self.subject = subject
    self.ticketType = ticketType
    self.content = content
    self.modified = NSDate()
  }
  
}

func < (lhs: SupportTicket, rhs: SupportTicket) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: SupportTicket, rhs: SupportTicket) -> Bool {
  return lhs._id == rhs._id
}

extension SupportTicket: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}

extension SupportTicket{
  func dataToSynchronize() -> [String: AnyObject]?{
    var dict : [String: AnyObject] = [String: AnyObject]()
    dict["category"] = self.ticketType
    dict["email"] = self.email
    dict["subject"] = self.subject
    dict["body"] = self.content
    return dict
  }
}

