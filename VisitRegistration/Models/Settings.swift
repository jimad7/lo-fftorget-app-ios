//
//  Settings.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 02/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

//
//  SupportTicket.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 24/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class Settings: NSObject, Storable, Comparable {
  var _id: Int = 0
  var synchronization: Bool = true
  var employer: String = ""
  var numberOfVisits: Int = 5
  var modified : NSDate? = NSDate()
  
  override required init() {
    super.init()
  }
  
  init?(attributes: [String : Any?]) {
    super.init()
    guard
      let _id = attributes["_id"] as? Int,
      let synchronization = attributes["synchronization"] as? Bool
      else {
        return nil
    }
 
    self._id = _id
    if(attributes["employer"] == nil){
        self.employer = ""
    }else{
        self.employer = (attributes["employer"] as? String)!
    }
    
    self.synchronization = synchronization
    if let visitCount = attributes["no_of_visits"] as? String {
      self.numberOfVisits = Int(visitCount)!
    } else if let visitCount = attributes["no_of_visits"] as? Int {
      self.numberOfVisits = visitCount
    }
    self.modified = NSDate()
  }
  
}

func < (lhs: Settings, rhs: Settings) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: Settings, rhs: Settings) -> Bool {
  return lhs._id == rhs._id
}

extension Settings: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}


