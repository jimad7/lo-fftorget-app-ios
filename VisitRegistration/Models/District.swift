//
//  District.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 08/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class District: NSObject, Storable, NSCoding, Comparable {
  var _id: Int = 0 // Notice that 'Int?' is not supported. Use NSNumber? instead
  var name: String?
    var unid: String?
  var modified : NSDate? = NSDate()
  
  override required init() {
    super.init()
  }
  
  init?(attributes: [String : AnyObject]) {
    super.init()
    guard let name = attributes["name"] as? String,
    let unid = attributes["id"] as? String    
      else {
        return nil
    }
    self.name = name
    self.unid = unid
  }
  
  //MARK: NSCoding
  required init(coder aDecoder: NSCoder) {
    self._id = aDecoder.decodeObjectForKey("id") as! Int
    self.name = aDecoder.decodeObjectForKey("name") as? String
    self.unid = aDecoder.decodeObjectForKey("unid") as? String
    self.modified = aDecoder.decodeObjectForKey("modified") as? NSDate
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(_id, forKey: "id")
    aCoder.encodeObject(unid, forKey: "unid")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeObject(modified, forKey: "modified")
  }
}

func < (lhs: District, rhs: District) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: District, rhs: District) -> Bool {
  return lhs._id == rhs._id
}

extension District: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}
