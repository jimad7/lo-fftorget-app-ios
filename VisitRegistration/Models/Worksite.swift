//
//  Worksite.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 06/02/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

import SwiftyDB

class Worksite: NSObject, Storable, Comparable {
  var _id: Int = 0
  var name: String?
  var employerid: NSNumber? //reference to Employer
  var streetaddress: String?
  var zipcode: String?
  var city: String?
  var unid: String?
  var modified : NSDate? = NSDate()
  
  override required init() {
    super.init()
  }
  
  init?(attributes: [String : AnyObject]) {
    super.init()
    guard let name = attributes["site"] as? String,
    let unid = (attributes["name"] as! String + "-" + name) as? String,
    let streetaddress = attributes["gatuadress"] as? String,
    let zipcode = attributes["postnr"] as? String,
    let city = attributes["ort"] as? String
      else {
        return nil
    }
    self.unid = unid
    self.name = name
    self.streetaddress = streetaddress
    self.zipcode = zipcode
    self.city = city
  }
}

func < (lhs: Worksite, rhs: Worksite) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: Worksite, rhs: Worksite) -> Bool {
  return lhs._id == rhs._id
}

extension Worksite: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}