//
//  DownloadData.swift
//  VisitRegistration
//
//  Created by Jimmie Andersson on 2016-11-29.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class DownloadData: NSObject, Storable, Comparable {
    var _id: Int = 0
    var unid: String?
    var lastDatadownload : NSDate? = NSDate()
    var modified : NSDate? = NSDate()
    
    override required init() {
        super.init()
    }
    
    init?(attributes: [String : Any?]) {
        super.init()
        guard
            let _id = attributes["_id"] as? Int,
            let unid = attributes["unid"] as? String,
            let lastDatadownload : NSDate? = NSDate()
            else {
                return nil
        }
        
        self._id = _id
        self.unid = unid
        self.lastDatadownload = NSDate()
        self.modified = NSDate()
    }
    
    
    
    init?(unid: String) {
        super.init()
        self.unid = unid
        
    }
    
    
    
}


func < (lhs: DownloadData, rhs: DownloadData) -> Bool {
    return lhs._id < rhs._id
}

func == (lhs: DownloadData, rhs: DownloadData) -> Bool {
    return lhs._id == rhs._id
}

extension DownloadData: PrimaryKeys {
    class func primaryKeys() -> Set<String> {
        return ["_id"]
    }
}
