//
//  InfoContainer.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 03/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation

import SwiftyDB

enum InfoType: String {
  case Single = "enskildinformation"
  case Group = "gruppinformation"
  case Support = "support"
  case Signup = "intresse"
}
enum InfoStatus: Int {
  case LocallySaved = 0
  case Synced = 1
}
class InfoContainer: NSObject {
  var _id: Int = 0
  var name: String?
  var status: Int = InfoStatus.LocallySaved.rawValue
  var dateString : String?
  var employer: String?
  var infotype: String = InfoType.Single.rawValue
  
  override required init() {
    super.init()
  }
  
  
  init?(object: AnyObject) {
    super.init()

    if let visit = object as? Visit  {
      self._id = visit._id
      self.name = visit.name
      self.employer = visit.employer
      self.infotype = visit.visittype
      self.dateString = visit.interviewdate?.stringFromDateWithFormat(Config.shortDateFormat)
    } else if let support = object as? SupportTicket  {
      self._id = support._id
      self.name = support.subject
      self.employer = ""
      self.infotype = InfoType.Support.rawValue
      self.dateString = support.modified?.stringFromDateWithFormat(Config.shortDateFormat)
    } else if let signup = object as? Signup {
      self._id = signup._id
      self.name = signup.name
      self.employer = ""
      self.infotype = InfoType.Signup.rawValue
      self.dateString = signup.modified?.stringFromDateWithFormat(Config.shortDateFormat)
    }
  }
}












