//
//  Signup.swift
//  VisitRegistration
//
//  Created by Christofer Kihlman on 2016-02-19.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class Signup: NSObject, Storable, Comparable{
  var _id: Int = 0
  var unid:String?
  var userid: String? //32-char
  var modified : NSDate? = NSDate()
  var status: Int = InfoStatus.LocallySaved.rawValue
  var name:String?
  var phone:String?
  var mail:String?
  var unionid:String?
  var member:Int = 0
  var city:String?
  var worksite:String?
  var comments:String?
  var visitid: Int = 0
  
  override required init(){
    super.init()
  }
  
  init?(attributes: [String : Any?]){
    super.init()
    guard
      let name = attributes["name"] as? String,
      let phone = attributes["phone"] as? String,
      let city = attributes["city"] as? String
      
      else{
        return nil
    }
    
    self.name = name
    self.phone = phone
    self.city = city
    if attributes["mail"] as? String != nil{
      self.mail = (attributes["mail"] as? String)!
    }
    
    //if attributes["member"] as? Bool != nil && (attributes["member"] as? Bool)! == true{
    if attributes["member"] as? String != nil && (attributes["member"] as? String)! == "Ja" {
      self.member = 1
    } else {
      self.member = 0
    }
    if attributes["worksite"] as? String != nil{
      self.worksite = (attributes["worksite"] as? String)!
    }
    if attributes["comments"] as? String != nil{
        self.comments = (attributes["comments"] as? String)!
    }
    if let id = attributes["_id"] as? Int {
      self._id = id
    }
    if let visitid = attributes["visitid"] as? Int {
      self.visitid = visitid
    }
    //self._id = (attributes["_id"] as? Int)!
    //let union = attributes["union"] as? DbUnion
    //self.unionid = union?.unid
    let unionid = attributes["union"] as? String
    self.unionid = unionid
    self.userid = ProfileData.id
    
  }
}
func < (lhs: Signup, rhs: Signup) -> Bool {
  return lhs._id < rhs._id
}

func == (lhs: Signup, rhs: Signup) -> Bool {
  return lhs._id == rhs._id
}

extension Signup: PrimaryKeys {
  class func primaryKeys() -> Set<String> {
    return ["_id"]
  }
}
extension Signup{
  func dataToSynchronize() -> [String: AnyObject]?{
    var dict : [String: AnyObject] = [String: AnyObject]()
    dict["id"] = self.userid
    dict["unionid"] = self.unionid
    dict["name"] = self.name
    dict["city"] = self.city
    dict["phone"] = self.phone
    dict["email"] = self.mail
    dict["member"] = String(self.member)
    dict["workplace"] = self.worksite
    dict["comments"] = self.comments
    return dict
  }
}
