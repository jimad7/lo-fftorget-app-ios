//
//  Profile.swift
//  VisitRegistration
//
//  Created by Johan Rudström on 09/03/16.
//  Copyright © 2016 Tromb AB. All rights reserved.
//

import Foundation
import SwiftyDB

class Profile: NSObject, Storable, Comparable {
    var _id: Int = 0 // Notice that 'Int?' is not supported. Use NSNumber? instead
    var name: String?
    var unid: String?
    var email: String?
    var phone: String?
    var dbunion: String?
    var dbunionid: String?
    var district: String?
    var districtid: String?
    var dbsection: String?
    var dbsectionid: String?
    var username: String?
    var fullname: String?
    var roles: String?
   // var lastDatadownload : NSDate? = NSDate()
    var modified : NSDate? = NSDate()
    
    override required init() {
        super.init()
    }
    
    init?(attributes: [String : AnyObject]) {
        super.init()
        guard let name = attributes["name"] as? String,
            let unid = attributes["user"] as? String,
            let email = attributes["email"] as? String,
            let phone = attributes["phone"] as? String,
            let union = attributes["union"] as? String,
            let district = attributes["district"] as? String,
            let dbunionid = attributes["unionid"] as? String,
            let dbsection = attributes["section"] as? String,
            let dbsectionid = attributes["sectionid"] as? String,
            let districtid = attributes["districtid"] as? String,
            let fullname = attributes["fullname"] as? String,
          //  let lastDatadownload = attributes["lastDatadownload"] as? NSDate,
            let roles = attributes["roles"] as? [String]
            else {
                return nil
        }
        
        self.name = name
        self.unid = unid
        self.email = email
        self.phone = phone
        self.dbunion = union
        self.district = district
        self.fullname = fullname
        self.dbunionid = dbunionid
        self.dbsection = dbsection
        self.dbsectionid = dbsectionid
        self.districtid = districtid
    //    self.lastDatadownload = lastDatadownload
        self.roles = roles.joinWithSeparator(",")
    }
    
    init?(username: String, unid: String) {
        super.init()
        
        self.unid = unid
        self.username = username
    }
}

func < (lhs: Profile, rhs: Profile) -> Bool {
    return lhs._id < rhs._id
}

func == (lhs: Profile, rhs: Profile) -> Bool {
    return lhs._id == rhs._id
}

extension Profile: PrimaryKeys {
    class func primaryKeys() -> Set<String> {
        return ["_id"]
    }
}
